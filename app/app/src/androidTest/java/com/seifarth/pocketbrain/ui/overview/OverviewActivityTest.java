package com.seifarth.pocketbrain.ui.overview;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;
import android.widget.ImageButton;

import com.seifarth.pocketbrain.R;

import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

@LargeTest
public class OverviewActivityTest extends ActivityInstrumentationTestCase2<OverviewActivity> {

    public OverviewActivityTest() {
        super(OverviewActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        // getting Activity under Test
        getActivity();
    }


    @Test
    public void test_showFloatingMenuWhenClicked() throws Exception {
        onView(withId(R.id.action_search)).perform(click());
//        onView(withId(R.id.fab1)).check(matches(isDisplayed()));
    }

    public void test_showDrawerWhenClickHamburgerIcon() throws Exception {
        onView(withId(R.id.overview_rl_drawer)).check(matches(not(isDisplayed())));
        onView(withId(R.id.toolbar)).onChildView(withId(R.id.item_size))
    }
}