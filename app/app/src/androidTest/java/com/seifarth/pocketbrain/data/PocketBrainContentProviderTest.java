package com.seifarth.pocketbrain.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.seifarth.pocketbrain.data.local.PocketBrainContentProvider;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.utils.LogUtils;

import static org.assertj.core.api.Assertions.*;

/**
 * Test class for testing PocketBrainContentProvider functionality against a isolated 
 * {@link android.content.ContentProvider} provided by {@link android.test.ProviderTestCase2} 
 * framework. Further information http://developer.android.com/tools/testing/contentprovider_testing.html*
 * <br><br>
 * Tests following
 * <ul>
 * <li>Testing provider using public contract.</li>
 * <li>Testing all offered URIs</li>
 * <li>Testing invalid URI</li>
 * <li>Testing standart provider functionality (query, insert, delete, update, getType, onCreate)</li>
 * <li>TODO testing business logic when needed* </li>
 * <li>TODO testing permissions when added* </li>
 *</ul>
 * Created by seifarth on 30.01.15.
 */
public class PocketBrainContentProviderTest extends ProviderTestCase2<PocketBrainContentProvider> {

    private static final String TAG = PocketBrainContentProviderTest.class.getName();

    private static PocketBrainContentProvider underTestContentProvider;


    public PocketBrainContentProviderTest(Class<PocketBrainContentProvider> providerClass, String providerAuthority) {
        super(providerClass, providerAuthority);
    }

    public PocketBrainContentProviderTest() {
        super(PocketBrainContentProvider.class, PocketBrainContract.AUTHORITY);
    }

    @Override
    public void setUp() throws Exception {
        LogUtils.test(TAG, "Setup PocketBrainContentProviderTest");
        super.setUp();
        underTestContentProvider = getProvider();

    }

    /**
     * Tests all possible UriTypes of PocketBrainContentProvider *
     * @throws Exception
     */
//    public void testGetUriTypesWithContract() throws Exception {
//        // test type categories
//        String categorieType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_CATEOGIRES);
//        assertThat(categorieType).isEqualTo(PocketBrainContract.CATEGOIES_TYPE);
//
//        // test type single categories
//        String singleCategorieType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_CATEOGIRES,"1"));
//        assertThat(singleCategorieType).isEqualTo(PocketBrainContract.SINGLE_CATEGOIE_TYPE);
//
//        // test type tags
//        String tagType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_TAGS);
//        assertThat(tagType).isEqualTo(PocketBrainContract.TAGS_TYPE);
//
//        // test type single tag
//        String singleTagType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_TAGS, "1"));
//        assertThat(singleTagType).isEqualTo(PocketBrainContract.SINGLE_TAG_TYPE);
//
//        // test type entries
//        String entriesType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_ENTRIES);
//        assertThat(entriesType).isEqualTo(PocketBrainContract.ENTRIES_TYPE);
//
//        // test type single entry
//        String singleEntryType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_ENTRIES, "1"));
//        assertThat(singleEntryType).isEqualTo(PocketBrainContract.SINGLE_ENTRY_TYPE);
//
//        // test link entries
//        String linkType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_TEXT);
//        assertThat(linkType).isEqualTo(PocketBrainContract.TEXT_TYPE);
//
//        // test link single entry
//        String singleLinkType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_TEXT, "1"));
//        assertThat(singleLinkType).isEqualTo(PocketBrainContract.SINGLE_TEXT_TYPE);
//
//        // test has_link entries
//        String hasContainsType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_CONTAIN);
//        assertThat(hasContainsType).isEqualTo(PocketBrainContract.CONTAINS_TYPE);
//
//        // test has_link single entry
//        String singleContainsLinkType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_CONTAIN,"1"));
//        assertThat(singleContainsLinkType).isEqualTo(PocketBrainContract.SINGLE_CONTAINS_TYPE);
//
//        // test hasTags entries
//        String hasTagsType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_HAS_TAGS);
//        assertThat(hasTagsType).isEqualTo(PocketBrainContract.HAS_TAGS_TYPE);
//
//        // test hasTags single entry
//        String singleHasTagsType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_HAS_TAGS, "1"));
//        assertThat(singleHasTagsType).isEqualTo(PocketBrainContract.SINGLE_HAS_TAG_TYPE);
//
//        // test hasTags entries
//        String entriesWithTagsType = underTestContentProvider.getType(PocketBrainContract.CONTENT_URI_ENTRIES_WITH_TAGS);
//        assertThat(entriesWithTagsType).isEqualTo(PocketBrainContract.ENTRIES_WITH_TAGS_TYPE);
//
//        // test hasTags single entry
//        String signleEntriesWithTagsType = underTestContentProvider.getType(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_ENTRIES_WITH_TAGS, "1"));
//        assertThat(signleEntriesWithTagsType).isEqualTo(PocketBrainContract.SINGLE_ENTRY_WITH_TAGS_TYPE);
//
//    }


    public void testGetExceptionForUnsupportedItemType() throws Exception {
        // get unsupported item type
        try
        {
            String hasTagsType = underTestContentProvider.getType(Uri.parse(PocketBrainContract.BASE_URI + "/unsupported"));
            fail("Should have thrown IllegalArgumentException ");
        }
        catch(IllegalArgumentException e)
        {
            // success
        }
    }


//    public void testInsertSingleCategorie() throws Exception {
//        ContentValues newCategorieEntry = new ContentValues();
//        newCategorieEntry.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "testName");
//        newCategorieEntry.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION, "testDescription");
//        Uri resultUri = underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_CATEOGIRES, newCategorieEntry);
//        assertThat(resultUri).isNotNull();
//    }

//    public void testInsertSingleTag() throws Exception {
//        ContentValues newTagEntry = new ContentValues();
//        newTagEntry.put(PocketBrainContract.TAG_COLLUMS.NAME, "testName");
//        Uri resultUri = underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_TAGS, newTagEntry);
//        assertThat(resultUri).isNotNull();
//    }

//    public void testInsertSingleLink() throws Exception {
//        ContentValues newLinkEntry = new ContentValues();
//        Uri resultUri = underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_TEXT, newLinkEntry);
//        assertThat(resultUri).isNotNull();
//    }

//    public void testInsertSingleEntry() throws Exception {
//        //add categories
//        ContentValues dummyCategorie = new ContentValues();
//        dummyCategorie.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "DummyName");
//        dummyCategorie.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION, "Dummy Descritpion");
//        underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_CATEOGIRES, dummyCategorie);
//
//
//        ContentValues newEntry = new ContentValues();
//        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "headline");
//        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "body");
//        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
//        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
//        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
//        Uri resultUri = underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_ENTRIES, newEntry);
//        assertThat(resultUri).isNotNull();
//    }
    
    

    public void testGetExceptionForInsertInUnssuportedContentUri() throws Exception {
        ContentValues newEntry = new ContentValues();
        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "testHeadline");
        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "testBody");
        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        newEntry.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        try {
            Uri resultUri = underTestContentProvider.insert(Uri.parse(PocketBrainContract.BASE_URI + "/unsupported"), newEntry);
            fail("Should have thrown IllegalArgumentException ");
        } catch(IllegalArgumentException e) {
            // success
        }
    }



//    public void testGetEntriesWithTags() throws Exception {
//        ContentValues categorie1 = new ContentValues();
//        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Softwareentwicklung");
//        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Fachvokabular für Softwareentwicklung, Datenbanken, Requirement Engineering, Tools");
//        underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_CATEOGIRES, categorie1);
//
//        ContentValues tag1 = new ContentValues();
//        ContentValues tag2 = new ContentValues();
//        ContentValues tag3 = new ContentValues();
//        tag1.put(PocketBrainContract.TAG_COLLUMS.NAME,"android");
//        tag2.put(PocketBrainContract.TAG_COLLUMS.NAME,"datenbanken");
//        tag3.put(PocketBrainContract.TAG_COLLUMS.NAME,"usability");
//        underTestContentProvider.bulkInsert(PocketBrainContract.CONTENT_URI_TAGS, new ContentValues[]{tag1,tag2,tag3});
//
//        ContentValues entry1 = new ContentValues();
//        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Hier ist eine Headline Entry1");
//        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist der Body für Entry1");
//        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
//        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
//        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
//        underTestContentProvider.insert(PocketBrainContract.CONTENT_URI_ENTRIES, entry1);
//
//        //add tags
//        ContentValues hasTag1 = new ContentValues();
//        ContentValues hasTag2 = new ContentValues();
//        ContentValues hasTag3 = new ContentValues();
//
//
//
//        // android <-> entry 1
//        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "android");
//        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
//
//        // datenbanken <-> entry 1
//        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "datenbanken");
//        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
//
//        // datenbanken <-> entry 1
//        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "usability");
//        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
//
//        underTestContentProvider.bulkInsert(PocketBrainContract.CONTENT_URI_HAS_TAGS, new ContentValues[]{hasTag1,hasTag2,hasTag3});
//
//        Cursor result = underTestContentProvider.query(
//                PocketBrainContract.CONTENT_URI_ENTRIES_WITH_TAGS,
//                null,
//                null,
//                null,
//                null);
//
//        assertThat(result.getCount()).isEqualTo(1);
//        result.moveToNext();
//        assertThat(result.getString(result.getColumnIndex("EntryTags"))).isEqualTo("android;datenbanken;usability");
//    }
    
    

}
