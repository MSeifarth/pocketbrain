package com.seifarth.pocketbrain.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.data.local.PocketBrainSqliteOpenHelper;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by seifarth on 30.01.15.
 */
public class DatabaseLogikTest extends AndroidTestCase{

    public static final int TESTDATA_CATEGORIE_COUNT = 3;
    public static final int TESTDATA_CONTAINS_LINK_COUNT = 4;
    public static final int TESTDATA_HAS_TAGS_COUNT = 6;
    
    private PocketBrainSqliteOpenHelper mSqliteOpenHelper;
    private SQLiteDatabase database;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //change Context to avoid overriding productive database.
        RenamingDelegatingContext renamedContext
                = new RenamingDelegatingContext(getContext(), "test_");
        mSqliteOpenHelper = new PocketBrainSqliteOpenHelper(renamedContext);
    }


//    public void testDeleteSingleCategorieElement() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.CATEGORIES, null,
//                null,
//                null,
//                null,
//                null,
//                null);
////        assertEquals(TESTDATA_CATEGORIE_COUNT,resultBeforDelete.getCount());
//        assertThat(resultBeforDelete.getCount()).isEqualTo(TESTDATA_CATEGORIE_COUNT);
//
//
//        // delete row and check again
//        String[] elementToDelete = new String[]{"1"};
//        int rowsDeleted = database.delete(PocketBrainContract.Tables.CATEGORIES,
//                PocketBrainContract.BASE_COLLUMS._ID + " = ?",
//                elementToDelete);
//
//        assertThat(rowsDeleted).isEqualTo(1);
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.CATEGORIES, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertThat(resultAfterDelete.getCount()).isEqualTo(TESTDATA_CATEGORIE_COUNT - 1);
//
//        Cursor resultForSingleSearch = database.query(PocketBrainContract.Tables.CATEGORIES, null,
//                PocketBrainContract.BASE_COLLUMS._ID + "= ?",
//                elementToDelete,
//                null,
//                null,
//                null);
//        assertEquals(0,resultForSingleSearch.getCount());
//    }

//    public void testInsertSingleCategorieElement() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.CATEGORIES, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CATEGORIE_COUNT,resultBeforDelete.getCount());
//
//        // insert row and check again
//        ContentValues newCategorieEntry = new ContentValues();
//        newCategorieEntry.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Testentry name");
//        newCategorieEntry.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION, "Testentry description");
//        long newRowId = database.insert(PocketBrainContract.Tables.CATEGORIES, null,newCategorieEntry);
//
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.CATEGORIES, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CATEGORIE_COUNT + 1,resultAfterDelete.getCount());
//    }



    


    public void testDeleteContainLinksAfterDeleteLink() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CONTAINS_LINK_COUNT,resultBeforDelete.getCount());
//
//        //delete link and check again
//        String[] elementToDelete = new String[]{"1"};
//        int rowsDeleted = database.delete(PocketBrainContract.Tables.CONTENT,
//                PocketBrainContract.BASE_COLLUMS._ID + " = ?",
//                elementToDelete);
//
//        assertEquals(1, rowsDeleted);
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CONTAINS_LINK_COUNT - 2, resultAfterDelete.getCount());
//
//        Cursor resultForQueryForDeletedEntry = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                PocketBrainContract.CONTAINS.CONTENT_ID + "= ?",
//                elementToDelete,
//                null,
//                null,
//                null);
//        assertEquals(0,resultForQueryForDeletedEntry.getCount());
    }

    public void testDeleteContainLinksAfterDeleteEntry() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CONTAINS_LINK_COUNT,resultBeforDelete.getCount());
//
//        //delete link and check again
//        String[] elementToDelete = new String[]{"1"};
//        int rowsDeleted = database.delete(PocketBrainContract.Tables.ENTRIES,
//                PocketBrainContract.BASE_COLLUMS._ID + " = ?",
//                elementToDelete);
//
//        assertEquals(1, rowsDeleted);
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_CONTAINS_LINK_COUNT - 2, resultAfterDelete.getCount());
//
//        Cursor resultForQueryForDeletedEntry = database.query(PocketBrainContract.Tables.CONTAINS, null,
//                PocketBrainContract.CONTAINS.ENTRY_ID + "= ?",
//                elementToDelete,
//                null,
//                null,
//                null);
//        assertEquals(0,resultForQueryForDeletedEntry.getCount());
    }




//    public void testDeleteHasTagsAfterDeleteTag() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_HAS_TAGS_COUNT,resultBeforDelete.getCount());
//
//        //delete link and check again
//        String[] elementToDelete = new String[]{"android"};
//        int rowsDeleted = database.delete(PocketBrainContract.Tables.TAGS,
//                PocketBrainContract.TAG_COLLUMS.NAME + " = ?",
//                elementToDelete);
//
//        assertEquals(1, rowsDeleted);
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_HAS_TAGS_COUNT - 2, resultAfterDelete.getCount());
//
//        Cursor resultForQueryForDeletedEntry = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                PocketBrainContract.HAS_TAGS_COLLUMS.TAG + "= ?",
//                elementToDelete,
//                null,
//                null,
//                null);
//        assertEquals(0,resultForQueryForDeletedEntry.getCount());
//    }

    public void testDeleteHasTagsAfterDeleteEntry() throws Exception {
//        database = mSqliteOpenHelper.getReadableDatabase();
//
//        setupDatabaseTestdata(database);
//
//        // check result before delete
//        Cursor resultBeforDelete = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_HAS_TAGS_COUNT,resultBeforDelete.getCount());
//
//        //delete link and check again
//        String[] elementToDelete = new String[]{"1"};
//        int rowsDeleted = database.delete(PocketBrainContract.Tables.ENTRIES,
//                PocketBrainContract.BASE_COLLUMS._ID + " = ?",
//                elementToDelete);
//
//        assertEquals(1, rowsDeleted);
//        Cursor resultAfterDelete = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                null,
//                null,
//                null,
//                null,
//                null);
//        assertEquals(TESTDATA_HAS_TAGS_COUNT - 2, resultAfterDelete.getCount());
//
//        Cursor resultForQueryForDeletedEntry = database.query(PocketBrainContract.Tables.HAS_TAGS, null,
//                PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID + "= ?",
//                elementToDelete,
//                null,
//                null,
//                null);
//        assertEquals(0,resultForQueryForDeletedEntry.getCount());
    }
    
    
    
    

    private void setupDatabaseTestdata(SQLiteDatabase database) {
        
        //add categories 
        ContentValues categorie1 = new ContentValues();
        ContentValues categorie2 = new ContentValues();
        ContentValues categorie3 = new ContentValues();
        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Softwareentwicklung");
        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Fachvokabular für Softwareentwicklung, Datenbanken, Requirement Engineering, Tools");
        categorie2.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Usability Engineering");
        categorie2.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Fachvokabular für Usability Engineering");
        categorie3.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Personen");
        categorie3.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Personen die man kennen gelernt hat und damit man sich an die Namen erinnern kann.");
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie1);
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie2);
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie3);
        
        //add tags
        ContentValues tag1 = new ContentValues();
        ContentValues tag2 = new ContentValues();
        ContentValues tag3 = new ContentValues();
        ContentValues tag4 = new ContentValues();
        ContentValues tag5 = new ContentValues();
        ContentValues tag6 = new ContentValues();
        tag1.put(PocketBrainContract.TAG_COLLUMS.NAME,"android");
        tag2.put(PocketBrainContract.TAG_COLLUMS.NAME,"datenbanken");
        tag3.put(PocketBrainContract.TAG_COLLUMS.NAME,"usability");
        tag4.put(PocketBrainContract.TAG_COLLUMS.NAME,"user experience");
        tag5.put(PocketBrainContract.TAG_COLLUMS.NAME,"faz");
        tag6.put(PocketBrainContract.TAG_COLLUMS.NAME,"itemis");
        database.insert(PocketBrainContract.Tables.TAGS, null, tag1);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag2);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag3);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag4);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag5);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag6);

        //add link stuff
        ContentValues link1 = new ContentValues();
        ContentValues link2 = new ContentValues();
        ContentValues link3 = new ContentValues();
        ContentValues link4 = new ContentValues();
        database.insert(PocketBrainContract.Tables.CONTENT, null, link1);
        database.insert(PocketBrainContract.Tables.CONTENT, null, link2);
        database.insert(PocketBrainContract.Tables.CONTENT, null, link3);
        database.insert(PocketBrainContract.Tables.CONTENT, null, link4);

        //add erntry
        ContentValues entry1 = new ContentValues();
        ContentValues entry2 = new ContentValues();
        ContentValues entry3 = new ContentValues();
        ContentValues entry4 = new ContentValues();
        ContentValues entry5 = new ContentValues();
        ContentValues entry6 = new ContentValues();
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Hier ist eine Headline Entry1");
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist der Body für Entry1");
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Hier ist eine Headline Entry2");
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist der Body für Entry2");
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Hier ist eine Headline Entry3");
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist der Body für Entry3");
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Name einer Person");
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist etwas für den Namen und zur PErson");
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 2);
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE, "Name einer Person 2");
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.BODY, "Hier ist etwas für den Namen und zur PErson 2");
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.CREATED, 1121231114320L);
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.MODIFIED, 1121231114320L);
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 2);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry1);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry2);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry3);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry4);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry5);
        
        //add tags
        ContentValues hasTag1 = new ContentValues();
        ContentValues hasTag2 = new ContentValues();
        ContentValues hasTag3 = new ContentValues();
        ContentValues hasTag4 = new ContentValues();
        ContentValues hasTag5 = new ContentValues();
        ContentValues hasTag6 = new ContentValues();
        ContentValues hasTag7 = new ContentValues();
        ContentValues hasTag8 = new ContentValues();


        // android <-> entry 1
        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "android");
        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
        
        // datenbanken <-> entry 1
        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "datenbanken");
        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);

        // android <-> entry 2
        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "android");
        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 2);

        // datenbanken <-> entry 2
        hasTag4.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "datenbanken");
        hasTag4.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 2);

        // faz <-> entry (person) 4
        hasTag5.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "faz");
        hasTag5.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 4);

        // itemis <-> entry (person) 5
        hasTag6.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "itemis");
        hasTag6.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 5);

        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag1);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag2);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag3);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag4);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag5);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag6);
        
        //add links
        ContentValues containsLink1 = new ContentValues();
        ContentValues containsLink2 = new ContentValues();
        ContentValues containsLink3 = new ContentValues();
        ContentValues containsLink4 = new ContentValues();
        ContentValues containsLink5 = new ContentValues();
        ContentValues containsLink6 = new ContentValues();
        
        // link1 <-> entry 1
        containsLink1.put(PocketBrainContract.CONTAINS.ENTRY_ID, 1);
        containsLink1.put(PocketBrainContract.CONTAINS.CONTENT_ID, 1);

        // link2 <-> entry 1
        containsLink2.put(PocketBrainContract.CONTAINS.ENTRY_ID, 1);
        containsLink2.put(PocketBrainContract.CONTAINS.CONTENT_ID, 2);

        // link1 <-> entry 2
        containsLink3.put(PocketBrainContract.CONTAINS.ENTRY_ID, 2);
        containsLink3.put(PocketBrainContract.CONTAINS.CONTENT_ID, 1);

        // link3 <-> entry 3
        containsLink4.put(PocketBrainContract.CONTAINS.ENTRY_ID, 3);
        containsLink4.put(PocketBrainContract.CONTAINS.CONTENT_ID, 3);

        database.insert(PocketBrainContract.Tables.CONTAINS, null, containsLink1);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, containsLink2);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, containsLink3);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, containsLink4);
    }
}
