package com.seifarth.pocketbrain.data.remote.syncProcessors.base;

import android.content.Context;

import com.seifarth.pocketbrain.application.PocketBrainApplication;

/**
 * Created by seifarth on 28.02.15.
 */
  public class BaseSyncProcessor {

    public BaseSyncProcessor(Context context) {
        PocketBrainApplication.get(context).objectGraph.inject(this);
    }

    public boolean syncData() {
        boolean isSuccessfull = true;

        syncInsertedDataFromServer();
        syncInsertedDataToServer();

        syncUpdatedDataToServer();
        syncUpdatedDataFromServer();

        syncDeletedDataToServer();
        syncDeleteDataFromServer();

        return isSuccessfull;
    };

    protected boolean syncInsertedDataFromServer() {
        return false;
    }

    protected boolean syncInsertedDataToServer() {
        return false;
    }

    protected boolean syncUpdatedDataToServer() {
        return false;
    }

    protected boolean syncUpdatedDataFromServer() {
        return false;
    }

    protected boolean syncDeletedDataToServer() {
        return false;
    }

    protected boolean syncDeleteDataFromServer() {
        return false;
    }
}
