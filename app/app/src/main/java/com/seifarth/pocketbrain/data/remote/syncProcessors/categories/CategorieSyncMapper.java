package com.seifarth.pocketbrain.data.remote.syncProcessors.categories;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.CategorieMapper;

/**
 * Created by seifarth on 13.03.15.
 */
public class CategorieSyncMapper extends BaseSyncMapper {

    private static final String CLASS_TO_SYNC = "categories";

    public CategorieSyncMapper() {
        super(new CategorieResponseToOperationsMapperBase(), new CategorieMapper());
    }

    @Override
    public String getClassToSync() {
        return CLASS_TO_SYNC;
    }
}
