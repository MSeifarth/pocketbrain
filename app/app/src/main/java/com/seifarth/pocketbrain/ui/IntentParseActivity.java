package com.seifarth.pocketbrain.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * This Activity is reachable from other activities. Handle incoming intents from widget (clicks, search request)
 * or Deep Linkin stuff.
 */
public class IntentParseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        if (intent == null || intent.getData() == null) {
            // no data --> finish the activity
            finish();
        }

        handleIntent(intent);

        // Finish this activity
        finish();
    }

    private void handleIntent(final Intent intent) {

    }

}
