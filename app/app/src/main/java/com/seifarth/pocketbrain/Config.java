package com.seifarth.pocketbrain;

/**
 * Created by seifarth on 20.01.15.
 */
public class Config {
    public static final boolean IS_DEVELOP = true;
    public static final boolean IS_DATABASE_TEST_DATA = false;
    public static final boolean RESET_SYNC_DATA_STATE = false;
}