package com.seifarth.pocketbrain.data.remote.syncProcessors.text;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.TextMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class TextSyncMapper extends BaseSyncMapper{

    private static final String classToSync = "text";

    public TextSyncMapper() {
        super(new TextResponseToOperationMapper(), new TextMapper());
    }

    @Override
    public String getClassToSync() {
        return classToSync;
    }
}
