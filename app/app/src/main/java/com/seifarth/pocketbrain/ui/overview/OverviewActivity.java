package com.seifarth.pocketbrain.ui.overview;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.ActivityOptions;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.application.PocketBrainApplication;
import com.seifarth.pocketbrain.authentication.AuthenticationConstants;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.data.local.PocketBrainSuggestionProvider;
import com.seifarth.pocketbrain.ui.BaseActivity;
import com.seifarth.pocketbrain.ui.SuggestionCursorAdapter;
import com.seifarth.pocketbrain.ui.entityDetail.DetailActivity;
import com.seifarth.pocketbrain.ui.entityDetail.DetailFragment;
import com.seifarth.pocketbrain.utils.LogUtils;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;


public class OverviewActivity extends BaseActivity implements IOnEntrySelectListener{

    private static final String STATE_FLOATING_BTN = "state_floating_btn";
    private static final String STATE_SEARCH_VIEW_FOCUS = "state_searchview_focus";

    private static final String TAG = OverviewActivity.class.getSimpleName();

    @InjectView(R.id.toolbar) protected Toolbar toolbar;

    @Optional @InjectView(R.id.overview_drawer_layout) protected DrawerLayout overview_drawer_layout;
    @InjectView(R.id.progressbar) protected SmoothProgressBar progressbar;
    @InjectView(R.id.overview_fb_floatingbutton2) protected FloatingActionMenu floatingActionButton;

    private ActionBarDrawerToggle mDrawerToogle;

    private SearchManager mSearchManager;

    @Inject
    LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver toogleProgressbarReceiver;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);

        mFragmentManager = getFragmentManager();

        setupFloatingMenu();

        if(overview_drawer_layout != null) {
            //setup drawer
            mDrawerToogle = new ActionBarDrawerToggle(this, overview_drawer_layout,toolbar, R.string.drawer_open,R.string.drawer_close) {

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }
            };
            overview_drawer_layout.setDrawerListener(mDrawerToogle); // Drawer Listener set to the Drawer toggle
            mDrawerToogle.syncState();
        }

        mSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);


        final AccountManager mAccountManager = AccountManager.get(this);
        
        final AccountManagerFuture<Bundle> future = mAccountManager.getAuthTokenByFeatures(
                AuthenticationConstants.ACCOUNT_TYPE,
                AuthenticationConstants.AUTHTOKEN_TYPE_FULL_ACCESS,
                null,
                this,
                null,
                null,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        Bundle bundle = null;
                        try {
                            bundle = future.getResult();
                            final String authtoken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                            LogUtils.debug(TAG, "GetTokenForAccount is " + bundle);
                            PocketBrainApplication.authToken = authtoken;
                            Account[] accounts = mAccountManager.getAccountsByType(AuthenticationConstants.ACCOUNT_TYPE);
                            if(accounts.length != 0) {
                                Account account = accounts[0];
                                String userObjectId = mAccountManager.getUserData(account, AuthenticationConstants.USERDATA_USER_OBJ_ID);
                                PocketBrainApplication.user.setObjectId(userObjectId);
                            }
                            
                        } catch (OperationCanceledException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (AuthenticatorException e) {
                            e.printStackTrace();
                        }
                    }
                },
                null
        );

        toogleProgressbarReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switchLoadingIndicator();
            }
        };
    }

    /**
     * setup all listener and stuff for floating button
     */
    private void setupFloatingMenu() {


        floatingActionButton.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                String text = "";
                if (opened) {
                    text = "Menu opened";
                } else {
                    text = "Menu closed";
                }
                Toast.makeText(OverviewActivity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocalBroadcastManager.registerReceiver(toogleProgressbarReceiver,new IntentFilter(BaseActivity.TOOGLE_PROGRESSBAR_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocalBroadcastManager.unregisterReceiver(toogleProgressbarReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_FLOATING_BTN, floatingActionButton.isOpened());
        LogUtils.debug(TAG, "OnSaveInstanceState : "+outState.toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.getBoolean(STATE_FLOATING_BTN)) {
            floatingActionButton.open(false);
        }
        LogUtils.debug(TAG, "onRestoreInstanceState : " + savedInstanceState.toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        LogUtils.debug(TAG, "onRestoreInstanceState with Bundle : " + savedInstanceState.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(mSearchManager.getSearchableInfo(OverviewActivity.this.getComponentName()));
        }

        setupSearchSuggestion(searchView);

        return super.onCreateOptionsMenu(menu);
    }



    private void setupSearchSuggestion(final SearchView searchView) {

        final SuggestionCursorAdapter adapter =  new SuggestionCursorAdapter(this,null,0, getApplicationContext());;

        searchView.setSearchableInfo(mSearchManager.getSearchableInfo(getComponentName()));
        searchView.setSuggestionsAdapter(adapter);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.debug(TAG, "clicked");
            }
        });
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {

            @Override
            public boolean onSuggestionSelect(int position) {
                LogUtils.debug(TAG, "selected");
                Object item = searchView.getSuggestionsAdapter().getItem(position);
                if (item instanceof Cursor) {
                    LogUtils.debug(TAG, "es ist ein cursor");
                }
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                LogUtils.debug(TAG, "clicked");
                Object item = searchView.getSuggestionsAdapter().getItem(position);
                if (item instanceof Cursor) {
                    Cursor cursor = (Cursor) item;
                    switch (cursor.getString(cursor.getColumnIndex(PocketBrainSuggestionProvider.SUGGESTION_TYPE))) {
                        case "entry":
                            Toast.makeText(getApplicationContext(), "entry", Toast.LENGTH_LONG).show();
                            return true;
                        case "categorie":
                            Toast.makeText(getApplicationContext(), "categorie", Toast.LENGTH_LONG).show();
                            return true;
                        case "tags":
                            Toast.makeText(getApplicationContext(), "tags", Toast.LENGTH_LONG).show();
                            return true;
                        case "recent":
                            Toast.makeText(getApplicationContext(), "recent", Toast.LENGTH_LONG).show();
                            return true;
                        default:
                            return false;
                    }
                }
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                LogUtils.debug(TAG, "on text submit");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                LogUtils.debug(TAG, "on text change");
                Cursor cursor = getContentResolver().query(Uri.withAppendedPath(PocketBrainContract.CONTENT_URI_SEARCH, s), null, null, null, null);
                if (cursor.getCount() > 0) {
                    adapter.changeCursor(cursor);
                    adapter.notifyDataSetChanged();
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_sync) {

            forceSync();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    /**
     * switched the Visible State of the LoadingIndicator
     */
    public void switchLoadingIndicator(){
        // the progressbar has to be final or you can't change the
        // visibility. I have no fucking idea why :D

        if(progressbar.getVisibility() == View.VISIBLE) {
            progressbar.progressiveStop();
            progressbar.setVisibility(View.INVISIBLE);
        } else {
            progressbar.setVisibility(View.VISIBLE);
            progressbar.progressiveStart();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSearchRequested() {
        LogUtils.debug(TAG, "onSearchRequested");
        return super.onSearchRequested();
    }

    @Override
    public void startSearch(String initialQuery, boolean selectInitialQuery, Bundle appSearchData, boolean globalSearch) {
        LogUtils.debug(TAG,"startSearch");
        super.startSearch(initialQuery, selectInitialQuery, appSearchData, globalSearch);
    }

    @Override
    public void onEntrySelected(OverviewCard card, View view) {
        DetailFragment detailFragment = (DetailFragment) mFragmentManager.findFragmentById(R.id.fragment2);
        if(detailFragment != null) {
            detailFragment.doSomething(card.getDbId());
        } else {
            // start intent
            Intent intent = new Intent(this, DetailActivity.class);

            String transitionName = "testTransition";
            View headlineView = view.findViewById(R.id.overviewcard_tv_headline);
            ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(this, headlineView, transitionName);
            intent.putExtra("headline", card.getTitle());
            startActivity(intent, transitionActivityOptions.toBundle());
        }
    }
}
