package com.seifarth.pocketbrain.authentication;

import com.seifarth.pocketbrain.model.User;


public interface IServerAuthenticate {
    public User userSignUp(final String name,String accountName, final String pass, String authType) throws Exception;

    public User userSignIn(final String user, final String pass, String authType) throws Exception;
    
    public boolean changePassword(final User user,final String newPassword, final String authToken) throws Exception;

    public boolean verifyAuthToken(final String authToken);
}
