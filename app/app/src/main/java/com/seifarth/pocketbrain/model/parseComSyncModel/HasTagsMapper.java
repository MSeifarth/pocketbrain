package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class HasTagsMapper extends BaseParseComObjectMapper {

    private String tag;
    private int entryId;

    @Override
    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        HasTagsMapper newTagsMapper = new HasTagsMapper();
        newTagsMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        newTagsMapper.setTag(cursor.getString(cursor.getColumnIndex(PocketBrainContract.HAS_TAGS_COLLUMS.TAG)));
        newTagsMapper.setEntryId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newTagsMapper.setDeleted(1);
        } else {
            newTagsMapper.setDeleted(0);

        }
        return newTagsMapper;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}
