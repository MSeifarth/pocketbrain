package com.seifarth.pocketbrain.data.remote.syncProcessors.entries;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.EntrieMapper;

/**
 * Created by seifarth on 13.03.15.
 */
public class EntrySyncMapper extends BaseSyncMapper {

    private static final String CLASS_TO_SYNC = "entries";

    public EntrySyncMapper() {
        super(new EntrieResponseToOperationsMapper(), new EntrieMapper());
    }

    @Override
    public String getClassToSync() {
        return CLASS_TO_SYNC;
    }
}
