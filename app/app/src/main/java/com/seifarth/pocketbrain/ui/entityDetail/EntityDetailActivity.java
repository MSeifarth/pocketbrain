package com.seifarth.pocketbrain.ui.entityDetail;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.ui.BaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by seifarth on 06.05.15.
 */
public class EntityDetailActivity extends ActionBarActivity {


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_entitydetail);
    }

}
