package com.seifarth.pocketbrain.ui.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.widget.ListView;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.data.local.PocketBrainRecentSuggestionsProvider;
import com.seifarth.pocketbrain.ui.BaseActivity;
import com.seifarth.pocketbrain.utils.LogUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by seifarth on 24.03.15.
 */
public class SearchResultActivity extends BaseActivity {

    private static final String TAG = SearchableActivity.class.getSimpleName();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.inject(this);
        handleIntent(getIntent());

        if(list != null) {
            LogUtils.debug(TAG,"injected");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query =
                    intent.getStringExtra(SearchManager.QUERY);

            //store query in suggestions database
            SearchRecentSuggestions suggestions =
                    new SearchRecentSuggestions(this,
                            PocketBrainRecentSuggestionsProvider.AUTHORITY,
                            PocketBrainRecentSuggestionsProvider.MODE);
            suggestions.saveRecentQuery(query, null);


            doSearch(query);
        }
    }

    private void doSearch(String query) {
        LogUtils.debug(TAG,"do search with query : "+query);
    }
}
