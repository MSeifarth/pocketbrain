package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class TextMapper extends BaseParseComObjectMapper {

    private int contentId;
    private String text;

    @Override
    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        TextMapper newTextMapper = new TextMapper();
        newTextMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        newTextMapper.setContentId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.TEXT_COLLUMS.CONTENT_ID)));
        newTextMapper.setText(cursor.getString(cursor.getColumnIndex(PocketBrainContract.TEXT_COLLUMS.TEXT)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newTextMapper.setDeleted(1);
        } else {
            newTextMapper.setDeleted(0);

        }
        return newTextMapper;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
