package com.seifarth.pocketbrain.ui.overview;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.ui.entityDetail.DetailActivity;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardCursorAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Cursor adapter for OverviewFragment creating cards from cursor content.
 */
public class OverviewFragmentListAdapter extends CardCursorAdapter implements StickyListHeadersAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private IOnEntrySelectListener mActivityCallback;

    public OverviewFragmentListAdapter(Context context) {
        super(context);
        this.context = context;
        mActivityCallback = (OverviewActivity) context;
        mInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected Card getCardFromCursor(Cursor cursor) {
        OverviewCard card = new OverviewCard(context);
        setCardFromCursor(card, cursor);
        return card;
    }




    @Override
    /**
     * get a View for the sticky header of the StickyHeaderListHeader
     */
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        // check if cursor is empty
        if (!getCursor().moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }

        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.headertext);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        String headerText = "" + getFirstCharFromCursorHeadline(getCursor());
        holder.text.setText(headerText);
        return convertView;
    }



    /**
     * enrich card model with data from current data from a Cursor
     * @param card the cardmodel to enrich
     * @param cursor the cursor. Have to be already at right position.
     */
    private void setCardFromCursor(OverviewCard card, final Cursor cursor) {
        card.setTitle(cursor.getString(cursor.getColumnIndex(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE)));
        card.setTags(cursor.getString(cursor.getColumnIndex("tags")));
        card.setPreview(cursor.getString(cursor.getColumnIndex("text_text")));
        card.setDbId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        card.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                mActivityCallback.onEntrySelected(((OverviewCard)card),view);
            }
        });
    }

    @Override
    public long getHeaderId(int position) {

        //check if cursor is emtpy
        if (!getCursor().moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }
        return getHeaderId(getCursor());
    }

    private long getHeaderId(Cursor cursor) {
        return getFirstCharFromCursorHeadline(cursor);
    }

    private char getFirstCharFromCursorHeadline(Cursor cursor) {
        String entryHeadline = cursor.getString(cursor.getColumnIndex(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE));
        return entryHeadline.subSequence(0, 1).charAt(0);
    }


    class HeaderViewHolder {
        TextView text;
    }






}
