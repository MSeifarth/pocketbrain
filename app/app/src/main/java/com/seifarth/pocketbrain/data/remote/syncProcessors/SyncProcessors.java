package com.seifarth.pocketbrain.data.remote.syncProcessors;

import android.content.ContentProviderClient;
import android.content.Context;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncProcessor;
import com.seifarth.pocketbrain.data.remote.syncProcessors.categories.CategorieSyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.content.ContentSyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.entries.EntrySyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.hasTags.HasTagsSyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.tags.TagsSyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.text.TextSyncMapper;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by seifarth on 13.03.15.
 */
public class SyncProcessors {

    private Set<BaseSyncProcessor> processors;

    public SyncProcessors(Context content, ContentProviderClient providerClient) {
        processors = new HashSet<BaseSyncProcessor>();
        processors.add(new ParseComSyncProcessor(content, providerClient,new CategorieSyncMapper()));
        processors.add(new ParseComSyncProcessor(content, providerClient,new EntrySyncMapper()));
        processors.add(new ParseComSyncProcessor(content, providerClient,new TagsSyncMapper()));
        processors.add(new ParseComSyncProcessor(content, providerClient,new HasTagsSyncMapper()));
        processors.add(new ParseComSyncProcessor(content, providerClient,new TextSyncMapper()));
        processors.add(new ParseComSyncProcessor(content, providerClient,new ContentSyncMapper()));
    }

    public Iterator<BaseSyncProcessor> getIterator() {
        return processors.iterator();
    }
}
