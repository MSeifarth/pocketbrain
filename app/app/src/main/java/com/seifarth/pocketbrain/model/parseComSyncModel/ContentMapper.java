package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class ContentMapper extends BaseParseComObjectMapper {

    private int position;
    private int entryId;
    private int type;
    private String text_text;
    private String image_url;

    @Override
    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        ContentMapper newContentMapper = new ContentMapper();
        newContentMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        newContentMapper.setPosition(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.CONTENT_COLLUMS.POSITION)));
        newContentMapper.setEntryId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID)));
        newContentMapper.setType(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.CONTENT_COLLUMS.CONTENT_TYPE)));
        newContentMapper.setText_text(cursor.getString(cursor.getColumnIndex(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT)));
        newContentMapper.setImage_url(cursor.getString(cursor.getColumnIndex(PocketBrainContract.CONTENT_COLLUMS.TYPE_IMAGE_URL)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newContentMapper.setDeleted(1);
        } else {
            newContentMapper.setDeleted(0);

        }
        return newContentMapper;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getText_text() {
        return text_text;
    }

    public void setText_text(String text_text) {
        this.text_text = text_text;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
