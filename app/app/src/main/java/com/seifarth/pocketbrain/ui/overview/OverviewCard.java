package com.seifarth.pocketbrain.ui.overview;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eyeem.chips.AwesomeBubble;
import com.eyeem.chips.BubbleSpanImpl;
import com.eyeem.chips.BubbleStyle;
import com.eyeem.chips.ChipsTextView;
import com.seifarth.pocketbrain.R;

import java.util.StringTokenizer;

import it.gmariotti.cardslib.library.internal.Card;


public class OverviewCard extends Card {

    private Context context;

    private String title;
    private String preview;
    private String tags;
    private long dbId;


    public OverviewCard(Context context) {
        super(context, R.layout.view_overviewcard);
        this.context = context;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        TextView titleTextView = (TextView) parent.findViewById(R.id.overviewcard_tv_headline);
        titleTextView.setText(title);

        TextView previewText = (TextView) parent.findViewById(R.id.overview_card_preview);
        previewText.setText(preview);

        ChipsTextView tagsView = (ChipsTextView) parent.findViewById(R.id.overviewcard_ctv_tags);

        tagsView.setText(getSpannedTags(tags));
    }

    private Spannable getSpannedTags(String tags) {

        CharSequence spannedCharSequence = "";

        StringTokenizer stringTokenizer = new StringTokenizer(tags, ";");

        TextPaint myPaint = new TextPaint();
        myPaint.setColor(context.getResources().getColor(R.color.pocket_brain_white));
        BubbleStyle style = BubbleStyle.build(context, R.style.overview_bubble_default_style);

        while (stringTokenizer.hasMoreTokens()) {
            String tag = stringTokenizer.nextToken();
            SpannableString spannedTag = new SpannableString(tag);
            BubbleSpanImpl tagBubble = new BubbleSpanImpl(new AwesomeBubble(
                    tag,
                    500,
                    style,
                    myPaint));
            spannedTag.setSpan(tagBubble, 0, tag.length(), 0);
            spannedCharSequence = TextUtils.concat(spannedCharSequence, "  ", spannedTag);
        }

        return SpannableStringBuilder.valueOf(spannedCharSequence);
    }

    public long getDbId() {
        return dbId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }
}
