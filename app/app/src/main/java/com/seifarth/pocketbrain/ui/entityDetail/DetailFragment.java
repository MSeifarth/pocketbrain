package com.seifarth.pocketbrain.ui.entityDetail;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;

import org.w3c.dom.Text;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by seifarth on 14.05.15.
 */
public class DetailFragment extends Fragment{

    @InjectView(R.id.test_tv_headline) protected TextView text;

    protected ContentResolver mResolver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_detail,container,false);
        ButterKnife.inject(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mResolver = getActivity().getContentResolver();
    }

    public void doSomething(long id) {
        Toast.makeText(getActivity(),"DO SOMETHING IN FRAGMENT "+id,Toast.LENGTH_SHORT).show();
        Cursor query = mResolver.query(PocketBrainContract.CONTENT_URI_ENTRIES, null, "_id = ?", new String[]{String.valueOf(id)}, null, null);
        query.moveToFirst();
        text.setText(query.getString(query.getColumnIndex(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE)));
    }
}
