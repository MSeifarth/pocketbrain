package com.seifarth.pocketbrain.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.application.PocketBrainApplication;
import com.seifarth.pocketbrain.authentication.AuthenticationConstants;
import com.seifarth.pocketbrain.authentication.PocketBrainParserServer;

/**
 * Created by seifarth on 21.02.15.
 */
public class ChangePasswordActivity extends ActionBarActivity {

    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmPassword;
    private Button changePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        
        oldPassword = (EditText) findViewById(R.id.change_et_oldPassword);
        newPassword = (EditText) findViewById(R.id.change_et_newPassword);
        confirmPassword = (EditText) findViewById(R.id.change_et_confirmPassword);
        
        changePassword = (Button) findViewById(R.id.change_btn_change);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });
    }

    private void changePassword() {

        new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                String password = newPassword.getText().toString();

                PocketBrainParserServer server = new PocketBrainParserServer();
                try {
                    boolean isChanged = server.changePassword(PocketBrainApplication.user,password,PocketBrainApplication.authToken);
                    if(isChanged) {
                        AccountManager accountManager = AccountManager.get(getApplicationContext());
                        Account[] accounts = accountManager.getAccountsByType(AuthenticationConstants.ACCOUNT_TYPE);
                        Account account = null;
                        if(accounts.length != 0) {
                            account = accounts[0];
                        }
                        if(account != null) {
                            accountManager.setPassword(account,password);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }
        }.execute();
    }
}
