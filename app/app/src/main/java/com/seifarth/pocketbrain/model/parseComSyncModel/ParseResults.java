package com.seifarth.pocketbrain.model.parseComSyncModel;

import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

import java.util.ArrayList;

/**
 * Created by seifarth on 07.03.15.
 */
public class ParseResults {

    public ParseResults(ArrayList<BaseParseComObjectMapper> results) {
        this.results = results;
    }

    private ArrayList<BaseParseComObjectMapper> results;
}
