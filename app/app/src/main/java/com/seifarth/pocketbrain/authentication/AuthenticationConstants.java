package com.seifarth.pocketbrain.authentication;

/**
 * Some global constants for the hole authentication and login process
 *
 * Created by seifarth on 16.02.15.
 */
public class AuthenticationConstants {

    public static final String ACCOUNT_TYPE = "com.seifarth.pocketbrain.full_access";

    public static final String USERDATA_USER_OBJ_ID = "userObjectId";   //Parse.com object id

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an PocketBrain Account";
}
