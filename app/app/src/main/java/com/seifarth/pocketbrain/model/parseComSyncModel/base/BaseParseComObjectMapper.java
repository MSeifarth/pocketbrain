package com.seifarth.pocketbrain.model.parseComSyncModel.base;

import android.database.Cursor;

/**
 * The ParserObjectMapper is a abstraction of the data structur on parse.com
 * and is needed to map json response to data.
 */
public abstract class BaseParseComObjectMapper {

    private String createdAt;
    private String updatedAt;
    private int deleted;
    private int appId;

    public abstract BaseParseComObjectMapper fromCursor(Cursor cursor);

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }
}
