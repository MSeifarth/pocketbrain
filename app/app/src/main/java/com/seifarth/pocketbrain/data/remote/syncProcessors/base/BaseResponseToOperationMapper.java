package com.seifarth.pocketbrain.data.remote.syncProcessors.base;

import android.content.ContentProviderOperation;
import android.net.Uri;

import com.google.gson.JsonParser;
import com.squareup.okhttp.Response;

import java.util.ArrayList;

/**
 * A BaseResponseToOperationMapper maps a given JSON {@link Response} to a ArrayList of {@Link ContentProviderOperation}
 * translate server activities via {@link android.content.ContentProvider} to the local database.
 */
abstract public class BaseResponseToOperationMapper {

    public static final String PARSE_COM_ID_NAME = "appId"; // _id representation on parse.com server (_id not allowed)

    /**
     * Maps the JSON {@link Response} for new (inserted on server) data
     * @param response the response to map
     * @param parser JsonParser to parse response
     * @return The ArrayList with maped Reponse data.
     */
    abstract public ArrayList<ContentProviderOperation> mapForInsert(Response response, JsonParser parser);

    /**
     * Maps the JSON {@link Response} for changed (updated on server) data
     * @param response the response to map
     * @param parser JsonParser to parse response
     * @return The ArrayList with maped Reponse data.
     */
    abstract public ArrayList<ContentProviderOperation> mapForUpdate(Response response, JsonParser parser);

    /**
     * Return the ContentUri for the content type we try to map.
     * @return The ContentUri for the content we try to map.
     */
    abstract public Uri getContentUri();


    /**
     * Maps the JSON {@link Response} for changed (updated) local data. Needed to map generated
     * parse.com objectId to the generation _id of the local database
     * @param insertedRows List of generation _id
     * @param response the response to map
     * @param parser JsonParser to parse response
     * @return The ArrayList with maped Reponse data.
     */
    public abstract ArrayList<ContentProviderOperation> mapForInsertedToServer(ArrayList<Integer> insertedRows, Response response, JsonParser parser);
}
