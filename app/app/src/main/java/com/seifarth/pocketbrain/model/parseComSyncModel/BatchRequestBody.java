package com.seifarth.pocketbrain.model.parseComSyncModel;

import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseBatchMethods;

import java.util.Set;

/**
 * Created by seifarth on 07.03.15.
 */
public class BatchRequestBody {
    
    private Set<BaseBatchMethods> requests;

    public BatchRequestBody(Set<BaseBatchMethods> requests) {
        this.requests = requests;
    }
}
