package com.seifarth.pocketbrain.network;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by seifarth on 13.03.15.
 */
@Module(
        complete = false,
        library = true
)
public class NetworkModule {

    @Provides @Singleton public OkHttpClient providesOkHttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.networkInterceptors().add(new StethoInterceptor());
        return okHttpClient;
    }

    @Provides @Singleton public Gson providesGson() {
        return new Gson();
    }

    @Provides @Singleton public JsonParser providesJsonParser() {
        return new JsonParser();
    }
}
