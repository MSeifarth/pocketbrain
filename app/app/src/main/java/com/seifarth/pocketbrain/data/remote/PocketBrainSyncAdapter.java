package com.seifarth.pocketbrain.data.remote;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.securepreferences.SecurePreferences;
import com.seifarth.pocketbrain.application.PocketBrainApplication;
import com.seifarth.pocketbrain.data.remote.syncProcessors.SyncProcessors;
import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncProcessor;
import com.seifarth.pocketbrain.ui.BaseActivity;
import com.seifarth.pocketbrain.utils.LogUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by seifarth on 09.02.15.
 */
public class PocketBrainSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = PocketBrainSyncAdapter.class.getSimpleName();
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final int MINUTES_DELAY = 5;
    public static final int HOUR_DELAY = 4;

    private final Context context;

    @Inject SecurePreferences mSecurePrefrence;
    @Inject LocalBroadcastManager mLocalBroadcastManager;

    public PocketBrainSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;

        final ObjectGraph syncGraph = ((PocketBrainApplication) context.getApplicationContext()).objectGraph.plus(new SyncModule(true));
        syncGraph.inject(this);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        boolean isSuccesfull = true;

        LogUtils.sync(TAG, "----- START SYNCING -------------------------------------------");
        LogUtils.sync(TAG, "last SyncDate : " + mSecurePrefrence.getString("lastSyncDate", "2000-01-01T00:00:00Z"));


        Iterator<BaseSyncProcessor> iterator = new SyncProcessors(context,provider).getIterator();
        while (iterator.hasNext()) {
            iterator.next().syncData();
        }

        if(isSuccesfull) {
            //set last synchronisation date
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.US);
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, MINUTES_DELAY);
            calendar.add(Calendar.HOUR, HOUR_DELAY);
            LogUtils.debug(TAG, "Successfully sync data on " + simpleDateFormat.format(calendar.getTime()));
            mSecurePrefrence.edit().putString("lastSyncDate",simpleDateFormat.format(calendar.getTime())).apply();

            // stop progressbar
            mLocalBroadcastManager.sendBroadcast(new Intent(BaseActivity.TOOGLE_PROGRESSBAR_ACTION));
        } else {
            LogUtils.sync(TAG, "sync failed.");
        }

        // Do other stuff to sync 
    }
}
