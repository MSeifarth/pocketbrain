package com.seifarth.pocketbrain.application;

import android.app.Application;
import android.support.v4.content.LocalBroadcastManager;

import com.seifarth.pocketbrain.data.PersistenceModule;
import com.seifarth.pocketbrain.data.remote.syncProcessors.ParseComSyncProcessor;
import com.seifarth.pocketbrain.network.NetworkModule;
import com.seifarth.pocketbrain.ui.entityDetail.EntityDetailActivity;
import com.seifarth.pocketbrain.ui.overview.OverviewActivity;
import com.seifarth.pocketbrain.ui.search.SearchResultActivity;
import com.seifarth.pocketbrain.ui.search.SearchableActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by seifarth on 12.03.15.
 */
@Module(
        injects = {
                PocketBrainApplication.class,
                OverviewActivity.class,
                EntityDetailActivity.class,
                SearchableActivity.class,
                SearchResultActivity.class,
                ParseComSyncProcessor.class
        },
        includes = {
                PersistenceModule.class,
                NetworkModule.class
        }
)
public class ApplicationModule {

    private PocketBrainApplication application;
    private LocalBroadcastManager localBroadcastManager;

    public ApplicationModule(PocketBrainApplication application) {
        this.application = application;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(application);
    }

    @Provides public Application provideApplication() {
        return application;
    }

    @Provides public LocalBroadcastManager localBroadcastManager() {
        return localBroadcastManager;
    }


}
