package com.seifarth.pocketbrain.gcm;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.securepreferences.SecurePreferences;
import com.seifarth.pocketbrain.authentication.PocketBrainParserServer;
import com.seifarth.pocketbrain.model.DeviceRegistrationModel;
import com.seifarth.pocketbrain.utils.LogUtils;
import com.seifarth.pocketbrain.utils.StringUtils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by seifarth on 18.03.15.
 */
public class GcmRegisterer {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String TAG = GcmRegisterer.class.getSimpleName();

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private Context context;
    private GoogleCloudMessaging mGcm;
    private SecurePreferences mSecurePreferences;
    private String senderId;
    private Gson mGson = new Gson();

    public GcmRegisterer(Context context, SecurePreferences securePreferences, String senderId) {
        this.context = context;
        this.mSecurePreferences = securePreferences;
        this.senderId = senderId;
    }


    public String  register() {
        String registrationId = StringUtils.EMPTY;
        if (checkPlayServices()) {
            mGcm = GoogleCloudMessaging.getInstance(context);
            registrationId = getRegistrationId(context);

            if (registrationId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

        return  registrationId;
    }

    private String getRegistrationId(Context context) {
        String registrationId = mSecurePreferences.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }

        int registeredVersion = mSecurePreferences.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground() {
        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                LogUtils.debug(TAG,"Registering new device");
                String msg = "";
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(context);
                    }
                    String registrationId = mGcm.register(senderId);
                    msg = "Device registered, registration ID=" + registrationId;

                    sendRegistrationIdToBackend(registrationId);

                    storeRegistrationId(context, registrationId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String s) {
                LogUtils.debug(TAG,"Device registered. RegId: "+s);
            }
        }.execute(new String());
    }

    private void storeRegistrationId(Context context, String regId) {
        int appVersion = getAppVersion(context);
        mSecurePreferences.edit()
                .putString(PROPERTY_REG_ID,regId)
                .putInt(PROPERTY_APP_VERSION, appVersion)
                .commit();
    }

    private void sendRegistrationIdToBackend(final String registrationId) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                OkHttpClient okHttpClient = new OkHttpClient();
                final RequestBody body = RequestBody.create(MediaType.parse("application/json"), mGson.toJson(new DeviceRegistrationModel(registrationId,senderId,new ArrayList<String>())));
                Request request = new Request.Builder()
                        .url("https://api.parse.com/1/installations")
                        .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                        .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                        .post(body)
                        .build();
                try {
                    final Response response = okHttpClient.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return StringUtils.EMPTY;
            }
        }.execute(registrationId);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
