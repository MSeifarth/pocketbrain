package com.seifarth.pocketbrain.data.remote.syncProcessors.hasTags;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.HasTagsMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class HasTagsSyncMapper extends BaseSyncMapper {

    private static final String classToSync = "has_tags";

    public HasTagsSyncMapper() {
        super(new HasTagsReponseToOperationMapper(), new HasTagsMapper());
    }

    @Override
    public String getClassToSync() {
        return classToSync;
    }
}
