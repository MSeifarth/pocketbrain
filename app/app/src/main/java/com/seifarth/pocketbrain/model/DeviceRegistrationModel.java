package com.seifarth.pocketbrain.model;

import java.util.ArrayList;

/**
 * Created by seifarth on 21.03.15.
 */
public class DeviceRegistrationModel {

    private static final String DEVICE_TYPE_ANDROID = "android";
    private static final String PUSD_TYPE_GCM = "gcm";

    private String deviceType;
    private String pushType;
    private String deviceToken;
    private String GCMSenderId;
    private ArrayList<String> channels;

    public DeviceRegistrationModel(String deviceToken, String gcmSenderId, ArrayList<String> channels) {
        this.deviceToken = deviceToken;
        GCMSenderId = gcmSenderId;
        this.channels = channels;
        this.deviceType = DEVICE_TYPE_ANDROID;
        this.pushType = PUSD_TYPE_GCM;
    }

    public static String getDEVICE_TYPE_ANDROID() {
        return DEVICE_TYPE_ANDROID;
    }

    public static String getPUSD_TYPE_GCM() {
        return PUSD_TYPE_GCM;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getGCMSenderId() {
        return GCMSenderId;
    }

    public void setGCMSenderId(String GCMSenderId) {
        this.GCMSenderId = GCMSenderId;
    }

    public ArrayList<String> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<String> channels) {
        this.channels = channels;
    }
}
