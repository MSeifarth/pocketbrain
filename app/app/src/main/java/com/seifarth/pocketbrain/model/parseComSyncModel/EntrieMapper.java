package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 13.03.15.
 */
public class EntrieMapper extends BaseParseComObjectMapper {

    private int categorieId;
    private String headline;

    @Override
    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        EntrieMapper newEntryMapper = new EntrieMapper();
        newEntryMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        newEntryMapper.setCategorieId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID)));
        newEntryMapper.setHeadline(cursor.getString(cursor.getColumnIndex(PocketBrainContract.ENTRIES_COLLUMS.HEADLINE)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newEntryMapper.setDeleted(1);
        } else {
            newEntryMapper.setDeleted(0);

        }
        return newEntryMapper;
    }

    public int getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(int categorieId) {
        this.categorieId = categorieId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }
}
