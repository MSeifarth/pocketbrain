package com.seifarth.pocketbrain.data.remote.syncProcessors.base;

import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 13.03.15.
 */
abstract public class BaseSyncMapper {

    private BaseResponseToOperationMapper reponseToOperationMapper;
    private BaseParseComObjectMapper parserObjectMapper;

    public BaseSyncMapper(BaseResponseToOperationMapper reponseToOperationMapper, BaseParseComObjectMapper parserObjectMapper) {
        this.reponseToOperationMapper = reponseToOperationMapper;
        this.parserObjectMapper = parserObjectMapper;
    }

    public BaseParseComObjectMapper getParserObjectMapper() {
        return parserObjectMapper;
    }

    public void setParserObjectMapper(BaseParseComObjectMapper parserObjectMapper) {
        this.parserObjectMapper = parserObjectMapper;
    }

    public BaseResponseToOperationMapper getReponseToOperationMapper() {
        return reponseToOperationMapper;
    }

    public void setReponseToOperationMapper(BaseResponseToOperationMapper reponseToOperationMapper) {
        this.reponseToOperationMapper = reponseToOperationMapper;
    }

    public abstract String getClassToSync();
}
