package com.seifarth.pocketbrain.authentication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


/**
 * Created by seifarth on 14.02.15.
 */
public class PocketBrainAuthenticatorService extends Service {
    
    @Override
    public IBinder onBind(Intent intent) {
        PocketBrainAccountAuthenticator pocketBrainAccountAuthenticator = new PocketBrainAccountAuthenticator(this);
        return pocketBrainAccountAuthenticator.getIBinder();
    }
}
