package com.seifarth.pocketbrain.data.local;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by seifarth on 25.03.15.
 */
public class PocketBrainRecentSuggestionsProvider extends SearchRecentSuggestionsProvider{

    private static final String TAG = PocketBrainRecentSuggestionsProvider.class.getSimpleName();

    public static final String AUTHORITY = "com.seifarth.provider.pocketbrainrecentsuggestions";

    public static final int MODE = DATABASE_MODE_QUERIES;

    public PocketBrainRecentSuggestionsProvider() {
        setupSuggestions(AUTHORITY,MODE);
    }
}
