package com.seifarth.pocketbrain.authentication;

import android.database.Cursor;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.seifarth.pocketbrain.model.User;
import com.seifarth.pocketbrain.utils.LogUtils;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by seifarth on 14.02.15.
 */
public class PocketBrainParserServer implements IServerAuthenticate{
    
    private static final String TAG = PocketBrainParserServer.class.getSimpleName();
    
    final static public String PARSE_SERVER_APP_ID = "nMEQc3BSbAX7WWEhJYBbFgT0ZnWVSWkWdm7vpYKL";
    final static public String PARSE_REST_API_KEY = "otq9XCCPWHaSE9AdxRYH5CXofapx3CBilo7xcTNZ";

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";

    /**
     * Return the basic headers to connect our app's parse.com details
     *
     * @return Lists of needed headers
     */
    public static List<Header> getAppParseComHeaders() {
        List<Header> ret = new ArrayList<Header>();
        ret.add(new BasicHeader("X-Parse-Application-Id", PARSE_SERVER_APP_ID));
        ret.add(new BasicHeader("X-Parse-REST-API-Key", PARSE_REST_API_KEY));
        return ret;
    }

    /**
     * Return the basic headers for parse and enriched with the user session token*
     * @param authToken the session token to enrich
     * @return List of the the headers
     */
    public static List<Header> getAppParseComHeaders(String authToken) {
        List<Header> ret = getAppParseComHeaders();
        ret.add(new BasicHeader("X-Parse-Session-Token", authToken));
        return ret;
    }

    
    @Override
    public User userSignUp(String name, String accountName, String pass, String authType) throws Exception {

        String url = "https://api.parse.com/1/users";

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        for (Header header : getAppParseComHeaders()) {
            httpPost.addHeader(header);
        }
        httpPost.addHeader("Content-Type", "application/json");

        String user = "{\"username\":\"" + name + "\",\"password\":\"" + pass + "\",\"phone\":\"415-392-0202\"}";
        HttpEntity entity = new StringEntity(user);
        httpPost.setEntity(entity);

        User createdUser = new User();
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseString = EntityUtils.toString(response.getEntity());

            if (response.getStatusLine().getStatusCode() != 201) {
                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
                throw new Exception("Error creating user[" + error.code + "] - " + error.error);
            }

            createdUser = new Gson().fromJson(responseString, User.class);
            createdUser.setEmail(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return createdUser;
    }

    @Override
    public User userSignIn(String user, String pass, String authType) throws Exception {

        LogUtils.debug(TAG, "LoginUser : " + user);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "https://api.parse.com/1/login";

        String query = null;
        try {
            query = String.format("%s=%s&%s=%s", "username", URLEncoder.encode(user, "UTF-8"), "password", pass);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        url += "?" + query;

        HttpGet httpGet = new HttpGet(url);

        for (Header header : getAppParseComHeaders()) {
            httpGet.addHeader(header);
        }

        HttpParams params = new BasicHttpParams();
        params.setParameter("username", user);
        params.setParameter("password", pass);
        httpGet.setParams(params);
//        httpGet.getParams().setParameter("username", user).setParameter("password", pass);

        try {
            HttpResponse response = httpClient.execute(httpGet);

            String responseString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
                throw new Exception("Error signing-in [" + error.code + "] - " + error.error);
            }

            User loggedUser = new Gson().fromJson(responseString, User.class);
            return loggedUser;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean changePassword(final User user,final String newPassword, final String authToken) throws Exception {
        LogUtils.debug(TAG, String.format("Updating User with UserId %s",user.getObjectId()));
       
        String url = "https://api.parse.com/1/users/" + user.getObjectId();
        
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        for (Header header : getAppParseComHeaders(authToken)) {
            httpPost.addHeader(header);
        }
        httpPost.addHeader("Content-Type", "application/json");

        String userJsonString = String.format("{'password': '%s'}",newPassword);
        HttpEntity entity = new StringEntity(userJsonString);
        httpPost.setEntity(entity);

        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseString = EntityUtils.toString(response.getEntity());

            if (response.getStatusLine().getStatusCode() != 200) {
//                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
//                throw new Exception("Error creating user[" + error.code + "] - " + error.error);
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }

    @Override
    public boolean verifyAuthToken(String authToken) {
        LogUtils.debug(TAG,String.format("validate authToken %s",authToken));

        String url = "https://api.parse.com/1/users/me";
        
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        for (Header header : getAppParseComHeaders(authToken)) {
            httpPost.addHeader(header);
        }
        httpPost.addHeader("Content-Type", "application/json");

        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseString = EntityUtils.toString(response.getEntity());

            if (response.getStatusLine().getStatusCode() != 200) {
                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    //TODO : packe methode ins interface
    public void updateCategories(Cursor categoriesToUpdate, String authToken) {
        
    }
    
    public void insertCategories(Cursor categoriesToInsert, String authToken) {
    }


    public static class ParseComError implements Serializable {
        public int code;
        public String error;
    }
}
