package com.seifarth.pocketbrain.data.local;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MergeCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * Created by seifarth on 24.03.15.
 */
public class PocketBrainSuggestionProvider extends ContentProvider {

    private static final int SUGGESTIONS_THRESHOLD = 3;

    public static final String SUGGESTION_TYPE = "suggestion_type";
    public static final String CURRENT_QUERY = "current_query";

    private static final String TAG = PocketBrainSuggestionProvider.class.getSimpleName();
    public static final String SUGG_TYPE_ENTRIE = "SUGG_TYPE_ENTRIE";
    private static final String SUGG_TYPE_TAG = "SUGG_TYPE_TAG";

    private PocketBrainSqliteOpenHelper mSqliteOpenHelper;

    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        LogUtils.debug(TAG, "Create SuggestionProvider");
        mSqliteOpenHelper = new PocketBrainSqliteOpenHelper(getContext()); // ContentProvider context :D
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = uri.getLastPathSegment();
        LogUtils.debug(TAG, "query for suggestionProvider: "+query);


        if (query.length() < SUGGESTIONS_THRESHOLD) {
            return getRecentSuggestions(query);
        }
        else {
            // get database
            try {
                database = mSqliteOpenHelper.getWritableDatabase();
            }
            catch (SQLiteException ex) {
                database = mSqliteOpenHelper.getReadableDatabase();
            }




            Cursor cursorEntries = getEntrySuggestions(query);
            Cursor cursorTags = getTagSuggestions(query);
            Cursor cursorCategories = getCategorieSuggestions(query);
            Cursor cursorFTContent = getFTSSuggestions(query);

//            Cursor cursorRecentSuggestions = getRecentSuggestions(query);

            Cursor mergedCursor = new MergeCursor(new Cursor[]{cursorEntries,cursorTags,cursorCategories});

            LogUtils.debug(TAG, "found suggestions : " + mergedCursor.getCount());

            return mergedCursor;
        }
    }

    private Cursor getFTSSuggestions(String query) {
        return null;
    }

    private Cursor getCategorieSuggestions(final String query) {
        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        //we need a projection map to map normal results to suggestion result
        final String[] projectionMapTags = new String[] {
                "_id",
                "'categorie'" +  " AS " + SUGGESTION_TYPE,
                "'"+query+"'" + " AS " + CURRENT_QUERY,
                PocketBrainContract.CATEGORIES_COLLUMS.NAME + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1,
                PocketBrainContract.CATEGORIES_COLLUMS.NAME + " AS " + SearchManager.SUGGEST_COLUMN_QUERY,
                PocketBrainContract.BASE_COLLUMS._ID+ " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                android.R.drawable.ic_menu_rotate + " AS " + SearchManager.SUGGEST_COLUMN_ICON_1,
        };

        queryBuilder.setTables(PocketBrainContract.Tables.CATEGORIES);

        final String selectionTags = " name LIKE ? ";

        return queryBuilder.query(database, projectionMapTags, selectionTags,
                new String[]{"%" + query + "%"}, null, null, null);
    }

    private Cursor getTagSuggestions(final String query) {

        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        //we need a projection map to map normal results to suggestion result
        final String[] projectionMapTags = new String[] {
                "_id",
                "'tags'" +  " AS " + SUGGESTION_TYPE,
                "'"+query+"'" + " AS " + CURRENT_QUERY,
                PocketBrainContract.TAG_COLLUMS.NAME + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1,
                PocketBrainContract.TAG_COLLUMS.NAME + " AS " + SearchManager.SUGGEST_COLUMN_QUERY,
                PocketBrainContract.BASE_COLLUMS._ID+ " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                android.R.drawable.ic_delete + " AS " + SearchManager.SUGGEST_COLUMN_ICON_1,
        };

        queryBuilder.setTables(PocketBrainContract.Tables.TAGS);

        final String selectionTags = " name LIKE ? ";

        return queryBuilder.query(database, projectionMapTags, selectionTags,
                new String[]{"%" + query + "%"}, null, null, null);
    }

    private Cursor getEntrySuggestions(final String query) {

        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        //we need a projection map to map normal results to suggestion result
        final String[] projectionMapEntry = new String[] {
                "_id",
                "'entry'" +  " AS " + SUGGESTION_TYPE,
                "'"+query+"'" + " AS " + CURRENT_QUERY,
                PocketBrainContract.ENTRIES_COLLUMS.HEADLINE + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1,
                PocketBrainContract.ENTRIES_COLLUMS.HEADLINE + " AS " + SearchManager.SUGGEST_COLUMN_QUERY,
                PocketBrainContract.BASE_COLLUMS._ID + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                android.R.drawable.ic_input_add + " AS " + SearchManager.SUGGEST_COLUMN_ICON_1,
        };

        queryBuilder.setTables(PocketBrainContract.Tables.ENTRIES);

        final String selectionEntry = " headline LIKE ? ";

        return queryBuilder.query(database, projectionMapEntry, selectionEntry,
                new String[]{"%"+query+"%"}, null, null, null);
    }

    /**
     * Returns a cursor (prepared for SearchView) with the recent search suggestions. The method uses a
     * {@link PocketBrainRecentSuggestionsProvider} to get recent suggestions.
     *
     *
     * @param query query to search for
     * @return cursor with suggestion
     */
    private Cursor getRecentSuggestions(String query) {

        final String[] projection = new String[] {
                "_id",
                "'recent'" +  " AS " + SUGGESTION_TYPE,
                "'"+query+"'" + " AS " + CURRENT_QUERY,
                "display1 AS " + SearchManager.SUGGEST_COLUMN_TEXT_1,
                "query AS " + SearchManager.SUGGEST_COLUMN_QUERY,
                android.R.drawable.ic_menu_recent_history + " AS " + SearchManager.SUGGEST_COLUMN_ICON_1,
        };

        Cursor cursor = getContext().getContentResolver().query(
                PocketBrainContract.CONTENT_URI_RECENT_SEARCH
                ,projection,"display1 LIKE ? "
                ,new String[]{"%"+query+"%"}
                ,null);

        LogUtils.debug(TAG, "found recent suggestions : " + cursor.getCount());
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
