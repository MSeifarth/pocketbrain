package com.seifarth.pocketbrain.ui.overview;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.ui.entityDetail.DetailActivity;
import com.seifarth.pocketbrain.utils.LogUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class OverviewFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String STATE_LIST_POSITION = "state_list_position";
    private static final String STATE_LIST_POSITION_FIRST_ELEMENT = "state_list_position_first_element";

    private static final String TAG = OverviewFragment.class.getSimpleName();

    @InjectView(R.id.overview_slhlv_list) protected StickyListHeadersListView stickyList;

    private OverviewFragmentListAdapter mAdapter;

    // The callbacks through which we will interact with the LoaderManager.
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    // The loader's unique id. Loader ids are specific to the Activity or
    // Fragment in which they reside.
    private static final int LOADER_ID = 19;

    private int position = 0;
    private int topAbstand = 0;

    private IOnEntrySelectListener mParentActivityCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_overview,container,false);
        ButterKnife.inject(this, fragmentView);

        mAdapter = new OverviewFragmentListAdapter(getActivity());
        stickyList.setAdapter(mAdapter);

        mCallbacks = this;

        LoaderManager lm = getLoaderManager();
        lm.initLoader(LOADER_ID, null, mCallbacks);

        if(savedInstanceState != null && savedInstanceState.getInt(STATE_LIST_POSITION) != 0) {
            Toast.makeText(getActivity(),"Wert: "+savedInstanceState.getInt(STATE_LIST_POSITION) + " Y-Wert: " +  savedInstanceState.getInt(STATE_LIST_POSITION_FIRST_ELEMENT),Toast.LENGTH_SHORT).show();
            if(stickyList != null) {
                position = savedInstanceState.getInt(STATE_LIST_POSITION);
                topAbstand = savedInstanceState.getInt(STATE_LIST_POSITION_FIRST_ELEMENT);
            }
        }

        return fragmentView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtils.debug("onActivityCreated", "onActivityCreated");
        // Force start background query to load sessions
         getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mParentActivityCallback = (OverviewActivity )activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.debug("LoaderDebug","onCreate");
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        int index = stickyList.getFirstVisiblePosition();
        LogUtils.debug("LoaderDebug","onSaveInstance .. position "+stickyList.getFirstVisiblePosition());
        outState.putInt(STATE_LIST_POSITION, stickyList.getFirstVisiblePosition());
        View c = stickyList.getChildAt(0);
        int scrolly = -c.getTop() + stickyList.getFirstVisiblePosition() * c.getHeight();
        outState.putInt(STATE_LIST_POSITION_FIRST_ELEMENT, scrolly);
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        LogUtils.debug("LoaderDebug","onCreateLoader Id: "+id );
        Loader<Cursor> loader = null;
        loader = new CursorLoader(getActivity(), PocketBrainContract.CONTENT_URI_ENTRIES_WITH_TAGS,
                null, null , null, PocketBrainContract.ENTRIES_DEFAULT_SORT_ORDER);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        LogUtils.debug("LoaderDebug", "onLoadFinished for loaderId " + loader.getId());
        if (getActivity() == null) {
            return;
        }
        mAdapter.swapCursor(data);
        stickyList.setSelectionFromTop(0,-topAbstand);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        LogUtils.debug("LoaderDebug","OnLoaderReset");
        mAdapter.swapCursor(null);
    }

}
