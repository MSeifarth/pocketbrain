package com.seifarth.pocketbrain.data.remote;

import android.app.Application;

import com.seifarth.pocketbrain.application.ApplicationModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by seifarth on 13.03.15.
 */
@Module(
        injects = {
                PocketBrainSyncService.class,
                PocketBrainSyncAdapter.class
        },
        addsTo = ApplicationModule.class
)
public class SyncModule {

    private boolean autoinitialized = true;

    public SyncModule(boolean autoinitialized) {
        this.autoinitialized = autoinitialized;
    }

    @Provides @Singleton public PocketBrainSyncAdapter provideSyncAdapter(Application context) {
        return new PocketBrainSyncAdapter(context, autoinitialized);
    }
}
