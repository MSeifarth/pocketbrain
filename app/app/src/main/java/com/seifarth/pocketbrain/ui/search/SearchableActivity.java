package com.seifarth.pocketbrain.ui.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.ui.BaseActivity;
import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * Created by seifarth on 23.03.15.
 */
@Deprecated
public class SearchableActivity extends BaseActivity{

    private static final String TAG = SearchableActivity.class.getSimpleName();


    @Override
    protected void onStart() {
        super.onStart();
        LogUtils.debug(TAG, "start");
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        LogUtils.debug(TAG, "creat");

        setContentView(R.layout.activity_searchable);

        handleIntent(getIntent());
    }


    @Override
    protected void onNewIntent(Intent intent) {
        // needed for backward compatibility
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        LogUtils.debug(TAG, "handle Intent");

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            LogUtils.debug(TAG, "Search Query : "+query);
            doSearch();
        }
    }

    private void doSearch() {
        // get a Cursor, prepare the ListAdapter
        // and set it
    }

}
