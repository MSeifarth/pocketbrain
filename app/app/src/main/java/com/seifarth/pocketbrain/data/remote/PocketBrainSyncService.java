package com.seifarth.pocketbrain.data.remote;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.seifarth.pocketbrain.application.PocketBrainApplication;
import com.seifarth.pocketbrain.utils.LogUtils;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by seifarth on 10.02.15.
 */
public class PocketBrainSyncService extends Service {

    private static ObjectGraph syncGraph;

    private static final String TAG = PocketBrainSyncService.class.getSimpleName();

    @Inject
    PocketBrainSyncAdapter sSyncAdapter;

    @Override
    public void onCreate() {
        syncGraph = ((PocketBrainApplication) getApplication() ).objectGraph.plus(new SyncModule(true));
        syncGraph.inject(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.debug(TAG, "bind SyncService");
        return sSyncAdapter.getSyncAdapterBinder();
    }
}