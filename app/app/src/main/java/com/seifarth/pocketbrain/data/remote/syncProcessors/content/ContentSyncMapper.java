package com.seifarth.pocketbrain.data.remote.syncProcessors.content;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.ContentMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class ContentSyncMapper extends BaseSyncMapper {

    private static final String classToSync = "content";

    public ContentSyncMapper() {
        super(new ContentResponseToOperationMapper(), new ContentMapper());
    }

    @Override
    public String getClassToSync() {
        return classToSync;
    }
}
