package com.seifarth.pocketbrain.ui;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.application.PocketBrainApplication;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by seifarth on 13.03.15.
 */
public class BaseActivity extends ActionBarActivity {

    public static final String TOOGLE_PROGRESSBAR_ACTION = "com.seifarth.pocketbrain.action.TOOGLE_PROGRESSBAR";

    BroadcastReceiver mToogleProgressbarReceiver;

    @Inject LocalBroadcastManager mLocalBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PocketBrainApplication) getApplication()).objectGraph.inject(this);

    }



    /**
     * forces a sync of the content to the remote database
     */
    protected void forceSync() {
        mLocalBroadcastManager.sendBroadcast(new Intent(TOOGLE_PROGRESSBAR_ACTION));

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(null, PocketBrainContract.AUTHORITY, bundle);
    }
}
