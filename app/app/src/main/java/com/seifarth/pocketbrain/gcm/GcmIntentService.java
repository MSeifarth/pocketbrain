package com.seifarth.pocketbrain.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.ui.overview.OverviewActivity;
import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * Created by seifarth on 21.03.15.
 */
public class GcmIntentService extends IntentService{

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    private static final String TAG = GcmIntentService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LogUtils.debug(TAG,"Start GcmService");
        Bundle bundle = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);

        if(!bundle.isEmpty()) {
            switch (messageType) {
                case GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE :
                    sendNotification("You got a Notification");
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR :
                    sendNotification("Send error: " + bundle.toString());
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_DELETED :
                    sendNotification("Deleted on Server: " + bundle.toString());
                    break;
                default:
                    break;
            }
        }

    }

    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, OverviewActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_udinic)
                        .setContentTitle("GCM Notification")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
