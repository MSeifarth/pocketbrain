package com.seifarth.pocketbrain.data.remote.syncProcessors.content;

import android.content.ContentProviderOperation;
import android.net.Uri;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseResponseToOperationMapper;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by seifarth on 14.03.15.
 */
public class ContentResponseToOperationMapper extends BaseResponseToOperationMapper {

    private static final Uri CONTENT_URI = PocketBrainContract.CONTENT_URI_CONTENT;

    @Override
    public ArrayList<ContentProviderOperation> mapForInsert(Response response, JsonParser parser) {
        final ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

        try {
            String requestBody;
            requestBody = response.body().string();
            final JsonArray resultsElements = parser.parse(requestBody).getAsJsonObject().getAsJsonArray("results");
            Iterator<JsonElement> iterator = resultsElements.iterator();
            while (iterator.hasNext()) {
                JsonObject jsonObject = iterator.next().getAsJsonObject();
                operations.add(ContentProviderOperation.newInsert(CONTENT_URI)
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.POSITION, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.POSITION).getAsInt())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID).getAsInt())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.CONTENT_TYPE, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.CONTENT_TYPE).getAsInt())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT).getAsString())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.TYPE_IMAGE_URL, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.TYPE_IMAGE_URL).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.PARSE_ID, jsonObject.get(PocketBrainContract.BASE_COLLUMS.PARSE_ID).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.CREATED_AT, jsonObject.get(PocketBrainContract.BASE_COLLUMS.CREATED_AT).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.UPDATED_AT, jsonObject.get(PocketBrainContract.BASE_COLLUMS.UPDATED_AT).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT)
                        .build());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return operations;
    }

    @Override
    public ArrayList<ContentProviderOperation> mapForUpdate(Response response, JsonParser parser) {
        final ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

        try {
            String requestBody;
            requestBody = response.body().string();
            final JsonArray resultsElements = parser.parse(requestBody).getAsJsonObject().getAsJsonArray("results");
            Iterator<JsonElement> iterator = resultsElements.iterator();
            while (iterator.hasNext()) {
                JsonObject jsonObject = iterator.next().getAsJsonObject();
                operations.add(ContentProviderOperation.newUpdate(
                        CONTENT_URI)
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.POSITION, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.POSITION).getAsInt())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID).getAsInt())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT).getAsString())
                        .withValue(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT, jsonObject.get(PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.PARSE_ID, jsonObject.get("objectId").getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT)
                        .withValue(PocketBrainContract.BASE_COLLUMS.CREATED_AT, jsonObject.get(PocketBrainContract.BASE_COLLUMS.CREATED_AT).getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.UPDATED_AT, jsonObject.get(PocketBrainContract.BASE_COLLUMS.UPDATED_AT).getAsString())
                        .withSelection(PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?", new String[]{jsonObject.get("objectId").getAsString()})
                        .build());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return operations;
    }

    @Override
    public Uri getContentUri() {
        return CONTENT_URI;
    }

    @Override
    public ArrayList<ContentProviderOperation> mapForInsertedToServer(ArrayList<Integer> insertedRows, Response response, JsonParser parser) {
        final ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
        try {
            String requestBody;
            requestBody = response.body().string();
            final JsonArray resultsElements = parser.parse(requestBody).getAsJsonArray();
            for(int i = 0; i < resultsElements.size(); i++ ) {
                JsonObject jsonObject = resultsElements.get(i).getAsJsonObject().get("success").getAsJsonObject();
                operations.add(ContentProviderOperation.newUpdate(
                        CONTENT_URI)
                        .withValue(PocketBrainContract.BASE_COLLUMS.PARSE_ID, jsonObject.get("objectId").getAsString())
                        .withValue(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT)
                        .withSelection(PocketBrainContract.BASE_COLLUMS._ID + " = ?", new String[]{String.valueOf(insertedRows.get(i))})
                        .build());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return operations;
    }
}
