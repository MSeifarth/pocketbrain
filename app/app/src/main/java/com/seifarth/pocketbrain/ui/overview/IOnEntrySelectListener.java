package com.seifarth.pocketbrain.ui.overview;

import android.view.View;

/**
 * Created by seifarth on 14.05.15.
 */
public interface IOnEntrySelectListener  {

    public void onEntrySelected(OverviewCard id, View view);
}
