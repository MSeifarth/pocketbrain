package com.seifarth.pocketbrain.ui.espressoTest;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.seifarth.pocketbrain.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class EspressoTest extends ActionBarActivity {

    @InjectView(R.id.espresse_text) protected TextView espressoText;
    @InjectView(R.id.espresso_btn) protected Button espressoBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espresso_test);
        ButterKnife.inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_espresso_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showToast(){
        Toast.makeText(this,"CLICKED!",Toast.LENGTH_SHORT).show();
    }
}
