package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 07.03.15.
 */
public class CategorieMapper extends BaseParseComObjectMapper {

    private String name;
    private String description;
    private int appId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        CategorieMapper newCategorieMapper = new CategorieMapper();
        newCategorieMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        newCategorieMapper.setName(cursor.getString(cursor.getColumnIndex(PocketBrainContract.CATEGORIES_COLLUMS.NAME)));
        newCategorieMapper.setDescription(cursor.getString(cursor.getColumnIndex(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION)));
        newCategorieMapper.setAppId(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newCategorieMapper.setDeleted(1);
        } else {
            newCategorieMapper.setDeleted(0);

        }
        return newCategorieMapper;
    }
}
