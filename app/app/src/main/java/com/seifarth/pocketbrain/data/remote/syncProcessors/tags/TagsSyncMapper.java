package com.seifarth.pocketbrain.data.remote.syncProcessors.tags;

import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.model.parseComSyncModel.TagsMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class TagsSyncMapper extends BaseSyncMapper{

    private static final String classToSync = "tags";

    public TagsSyncMapper() {
        super(new TagsResponseToOperationsMapper(), new TagsMapper());
    }

    @Override
    public String getClassToSync() {
        return classToSync;
    }
}
