package com.seifarth.pocketbrain.ui.authentication;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.authentication.AuthenticationConstants;
import com.seifarth.pocketbrain.authentication.PocketBrainParserServer;
import com.seifarth.pocketbrain.model.User;
import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * The Activity to sign up the user.
 *  
 * Created by seifarth on 11.02.15.
 */
public class SignUpActivity extends Activity{

    private String TAG = SignUpActivity.class.getSimpleName();
    private String mAccountType;
    private EditText signup_et_name;
    private EditText signup_et_password;
    private Button sign_btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccountType = getIntent().getStringExtra(LoginActivity.ARG_ACCOUNT_TYPE);

        setContentView(R.layout.activity_signin);
        signup_et_name = (EditText) findViewById(R.id.signin_et_name);
        signup_et_password = (EditText) findViewById(R.id.signin_et_password);
        sign_btn_submit = (Button) findViewById(R.id.signin_btn_signup);

        findViewById(R.id.signin_btn_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
    }

    private void createAccount() {
        
        new AsyncTask<String, Void, Intent>() {
            
        final String username = signup_et_name.getText().toString();
        final String accountName = signup_et_name.getText().toString();
        final String password = signup_et_password.getText().toString();
            
            @Override
            protected Intent doInBackground(String... params) {
                
                LogUtils.debug(TAG, "Start SignUp....");

                String authtoken = null;
                Bundle data = new Bundle();
                try {
                    // try to get user fro
                    PocketBrainParserServer server = new PocketBrainParserServer();
                    User user = server.userSignUp(username, accountName, password, PocketBrainParserServer.AUTHTOKEN_TYPE_FULL_ACCESS);
                    
                    if (user != null) {
                        authtoken = user.getSessionToken();
                    }

                    data.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType);
                    data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);

                    // We keep the user's object id as an extra data on the account.
                    // It's used later for determine ACL for the data we send to the Parse.com service
                    Bundle userData = new Bundle();
                    userData.putString(AuthenticationConstants.USERDATA_USER_OBJ_ID, user.getObjectId());
                    data.putBundle(AccountManager.KEY_USERDATA, userData);

                    data.putString(LoginActivity.PARAM_USER_PASS, password);
                } catch (Exception e) {
                    data.putString(LoginActivity.KEY_ERROR_MESSAGE, e.getMessage());
                }

                final Intent res = new Intent();
                res.putExtras(data);
                return res;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                if (intent.hasExtra(LoginActivity.KEY_ERROR_MESSAGE)) {
                    Toast.makeText(getBaseContext(), intent.getStringExtra(LoginActivity.KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                } else {
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
