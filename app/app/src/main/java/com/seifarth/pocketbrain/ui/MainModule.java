package com.seifarth.pocketbrain.ui;

import com.seifarth.pocketbrain.application.ApplicationModule;
import com.seifarth.pocketbrain.ui.overview.OverviewActivity;
import com.seifarth.pocketbrain.ui.search.SearchableActivity;

import dagger.Module;

/**
 * Created by seifarth on 12.03.15.
 */
@Module(
        injects = {
                OverviewActivity.class,
                BaseActivity.class,
                SearchableActivity.class
        },
        addsTo = ApplicationModule.class
)
public class MainModule {
}
