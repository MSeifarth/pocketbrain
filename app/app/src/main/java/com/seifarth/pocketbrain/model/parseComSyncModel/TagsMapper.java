package com.seifarth.pocketbrain.model.parseComSyncModel;

import android.database.Cursor;

import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 14.03.15.
 */
public class TagsMapper extends BaseParseComObjectMapper {

    private String name;

    @Override
    public BaseParseComObjectMapper fromCursor(Cursor cursor) {
        TagsMapper newTagsMapper = new TagsMapper();
        newTagsMapper.setName(cursor.getString(cursor.getColumnIndex(PocketBrainContract.TAG_COLLUMS.NAME)));
        if(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE)) == PocketBrainContract.IS_DELETING) {
            newTagsMapper.setDeleted(1);
        } else {
            newTagsMapper.setDeleted(0);

        }
        return newTagsMapper;
    }

    public String getName() {
        return name;
    }

    public void setName(String tag) {
        this.name = tag;
    }
}
