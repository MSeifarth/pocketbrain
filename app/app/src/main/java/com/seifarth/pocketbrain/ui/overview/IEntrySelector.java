package com.seifarth.pocketbrain.ui.overview;

/**
 * Created by seifarth on 14.05.15.
 */
public interface IEntrySelector {

    public void entrySelected(int id);
}
