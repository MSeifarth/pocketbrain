package com.seifarth.pocketbrain.data.local;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * Created by seifarth on 30.01.15.
 */
public class PocketBrainContentProvider extends ContentProvider {

    private static final String TAG = PocketBrainContentProvider.class.getSimpleName();

    private static final int CATEGORIES = 1;
    private static final int SINGLE_CATEGORIE = 2;
    private static final int TAGS = 3;
    private static final int SINGLE_TAG = 4;
    private static final int ENTRIES = 5;
    private static final int SINGLE_ENTRY = 6;
    private static final int CONTENT = 7;
    private static final int SINGLE_CONTENT = 8;
    private static final int CONTAINS = 9;
    private static final int SINGLE_CONTAINS = 10;
    private static final int HAS_TAGS = 11;
    private static final int SINGLE_HAS_TAG = 12;
    
    private static final int ENTRIES_WITH_TAGS = 13;
    private static final int SINGLE_ENTRY_WITH_TAGS = 14;
    private static final int TEXT = 15;
    private static final int SINGLE_TEXT = 16;

    private static final int SEARCH_QUERY = 99;


    private PocketBrainSqliteOpenHelper mSqliteOpenHelper;
    private SQLiteDatabase database;
    private static final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);




    // The # sign will match numbers
    static {
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "categories", CATEGORIES);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "categories/#", SINGLE_CATEGORIE);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "entries", ENTRIES);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "entries/#", SINGLE_ENTRY);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "hastags", HAS_TAGS);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "hastags/#", SINGLE_HAS_TAG);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "tags", TAGS);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "tags/#", SINGLE_TAG);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "contains", CONTAINS);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "contains/#", SINGLE_CONTAINS);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "content", CONTENT);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "content/#", SINGLE_CONTENT);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "text", TEXT);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "text/#", SINGLE_TEXT);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "searchMe/"+ SearchManager.SUGGEST_URI_PATH_QUERY+"/#", SEARCH_QUERY);
        
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "entriesWithTags", ENTRIES_WITH_TAGS);
        mUriMatcher.addURI(PocketBrainContract.AUTHORITY, "entriesWithTags/#", SINGLE_ENTRY_WITH_TAGS);
    }
    
    @Override
    public boolean onCreate() {
        LogUtils.debug(TAG, "Create PocketBrainContentProvider");
        mSqliteOpenHelper = new PocketBrainSqliteOpenHelper(getContext()); // ContentProvider context :D
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        LogUtils.debug(TAG, "Run ContentProvider Query");

        try {
            database = mSqliteOpenHelper.getWritableDatabase();
        }
        catch (SQLiteException ex) {
            database = mSqliteOpenHelper.getReadableDatabase();
        }

        // Replace these with valid SQL statements if necessary.
        String groupBy = null;
        String having = null;

        // Use an SQLite Query Builder to simplify constructing the // database query.
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // If this is a row query, limit the result set to the passed in row.
        switch (mUriMatcher.match(uri)) {
            case SINGLE_CATEGORIE :
                String rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere("_id" + "=" + rowID);
                queryBuilder.setTables(PocketBrainContract.Tables.CATEGORIES);
                break;
            case CATEGORIES :
                queryBuilder.setTables(PocketBrainContract.Tables.CATEGORIES);
                break;
            case SINGLE_ENTRY :
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.ENTRIES);
                break;
            case ENTRIES :
                queryBuilder.setTables(PocketBrainContract.Tables.ENTRIES);
                break;
            case SINGLE_ENTRY_WITH_TAGS :
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.ENTRIES);
                break;
            case ENTRIES_WITH_TAGS :
                Cursor myCusor = database.rawQuery(
                        "SELECT entries.*, IFNULL(GROUP_CONCAT(has_tags.tag, \";\"),\"\") AS tags, content.text_text " +
                        "FROM entries " +
                        "LEFT JOIN has_tags ON entries._id = has_tags.entryId " +
                        "LEFT JOIN content ON entries._id = content.entryId AND content.position == (SELECT MIN(content.position) FROM content WHERE content.entryId = entries._id) " +
                        "GROUP BY entries._id ORDER BY entries.headline;",new String[]{});
                return myCusor;
            case SINGLE_HAS_TAG :
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.HAS_TAGS);
                break;
            case HAS_TAGS :
                queryBuilder.setTables(PocketBrainContract.Tables.HAS_TAGS);
                break;
            case SINGLE_TAG :
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.TAGS);
                break;
            case TAGS :
                queryBuilder.setTables(PocketBrainContract.Tables.TAGS);
                break;
            case SINGLE_CONTAINS:
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.CONTAINS);
                break;
            case CONTAINS:
                queryBuilder.setTables(PocketBrainContract.Tables.CONTAINS);
                break;
            case SINGLE_CONTENT:
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.CONTENT);
                break;
            case CONTENT:
                queryBuilder.setTables(PocketBrainContract.Tables.CONTENT);
                break;
            case SINGLE_TEXT:
                queryBuilder.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                queryBuilder.setTables(PocketBrainContract.Tables.TEXT);
                break;
            case TEXT:
                queryBuilder.setTables(PocketBrainContract.Tables.TEXT);
                break;
            case SEARCH_QUERY:
                LogUtils.debug(TAG,"query search");
                break;
            default:
                LogUtils.debug(TAG, "Another Query");
                throw new IllegalArgumentException("Unsupported URI:" +uri);
                }

        // Execute the query.
        Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, groupBy, having, sortOrder);


        
        
        // Return the result Cursor.
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        
        switch (mUriMatcher.match(uri)) {
            case CATEGORIES:
                return PocketBrainContract.CATEGOIES_TYPE;
            case SINGLE_CATEGORIE:
                return PocketBrainContract.SINGLE_CATEGOIE_TYPE;
            case TAGS:
                return PocketBrainContract.TAGS_TYPE;
            case SINGLE_TAG:
                return PocketBrainContract.SINGLE_TAG_TYPE;
            case ENTRIES:
                return PocketBrainContract.ENTRIES_TYPE;
            case SINGLE_ENTRY:
                return PocketBrainContract.SINGLE_ENTRY_TYPE;
            case CONTENT:
                return PocketBrainContract.TEXT_TYPE;
            case SINGLE_CONTENT:
                return PocketBrainContract.SINGLE_TEXT_TYPE;
            case CONTAINS:
                return PocketBrainContract.CONTAINS_TYPE;
            case SINGLE_CONTAINS:
                return PocketBrainContract.SINGLE_CONTAINS_TYPE;
            case HAS_TAGS:
                return PocketBrainContract.HAS_TAGS_TYPE;
            case SINGLE_HAS_TAG:
                return PocketBrainContract.SINGLE_HAS_TAG_TYPE;
            case ENTRIES_WITH_TAGS:
                return PocketBrainContract.ENTRIES_WITH_TAGS_TYPE;
            case SINGLE_ENTRY_WITH_TAGS:
                return PocketBrainContract.SINGLE_ENTRY_WITH_TAGS_TYPE;
            case TEXT:
                return PocketBrainContract.TEXT_TYPE;
            case SINGLE_TEXT:
                return PocketBrainContract.SINGLE_TEXT_TYPE;
            case SEARCH_QUERY:
                return PocketBrainContract.SEARCH_RESULTS_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI:" +uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        database = mSqliteOpenHelper.getWritableDatabase();

        long newRowId = -1;
        Uri newUri = Uri.EMPTY;

        LogUtils.debug(TAG, "Insert in Database");

        switch (mUriMatcher.match(uri)) {
            case CATEGORIES:
                newRowId = database.insert(PocketBrainContract.Tables.CATEGORIES, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_CATEOGIRES, newRowId);
                break;
            case TAGS:
                newRowId = database.insert(PocketBrainContract.Tables.TAGS, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_TAGS, newRowId);
                break;
            case ENTRIES:
                newRowId = database.insert(PocketBrainContract.Tables.ENTRIES, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_ENTRIES, newRowId);
                break;
            case CONTENT:
                newRowId = database.insert(PocketBrainContract.Tables.CONTENT, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_TEXT, newRowId);
                break;
            case CONTAINS:
                newRowId = database.insert(PocketBrainContract.Tables.CONTAINS, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_CONTAIN, newRowId);
                break;
            case HAS_TAGS:
                newRowId = database.insert(PocketBrainContract.Tables.HAS_TAGS, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_HAS_TAGS, newRowId);
                break;
            case TEXT:
                newRowId = database.insert(PocketBrainContract.Tables.TEXT, null, values);
                newUri = ContentUris.withAppendedId(PocketBrainContract.CONTENT_URI_TEXT, newRowId);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }

        // If record is added successfully
        if(newRowId == -1) {
            throw new SQLException("Fail to add a new record into " + uri);
        }

        // notify datachange
        getContext().getContentResolver().notifyChange(newUri, null);
        return newUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        database = mSqliteOpenHelper.getWritableDatabase();

        int rowsDeleted = 0;

        LogUtils.debug(TAG, "Delete Entry");

        switch (mUriMatcher.match(uri)) {
            case CATEGORIES:
                rowsDeleted = database.delete(PocketBrainContract.Tables.CATEGORIES, selection, selectionArgs);
                break;
            case SINGLE_CATEGORIE:
                rowsDeleted = database.delete( PocketBrainContract.Tables.CATEGORIES, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND (" 
                                + selection + ')' : ""), selectionArgs);
                break;
            case TAGS:
                rowsDeleted = database.delete(PocketBrainContract.Tables.TAGS, selection, selectionArgs);
                break;
            case SINGLE_TAG:
                rowsDeleted = database.delete( PocketBrainContract.Tables.TAGS, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            case ENTRIES:
                rowsDeleted = database.delete(PocketBrainContract.Tables.ENTRIES, selection, selectionArgs);
                break;
            case SINGLE_ENTRY:
                rowsDeleted = database.delete( PocketBrainContract.Tables.ENTRIES, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            case CONTENT:
                rowsDeleted = database.delete(PocketBrainContract.Tables.CONTENT, selection, selectionArgs);
                break;
            case SINGLE_CONTENT:
                rowsDeleted = database.delete( PocketBrainContract.Tables.CONTENT, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            case CONTAINS:
                rowsDeleted = database.delete(PocketBrainContract.Tables.CONTAINS, selection, selectionArgs);
                break;
            case SINGLE_CONTAINS:
                rowsDeleted = database.delete( PocketBrainContract.Tables.CONTAINS, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            case HAS_TAGS:
                rowsDeleted = database.delete(PocketBrainContract.Tables.HAS_TAGS, selection, selectionArgs);
                break;
            case SINGLE_HAS_TAG:
                rowsDeleted = database.delete( PocketBrainContract.Tables.HAS_TAGS, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            case TEXT:
                rowsDeleted = database.delete(PocketBrainContract.Tables.TEXT, selection, selectionArgs);
                break;
            case SINGLE_TEXT:
                rowsDeleted = database.delete( PocketBrainContract.Tables.TEXT, PocketBrainContract.BASE_COLLUMS._ID
                        +  " = " + uri.getLastPathSegment()
                        + (!TextUtils.isEmpty(selection) ? " AND ("
                        + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI:" +uri);

        }
        
        if(rowsDeleted > 0 ) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        
        return rowsDeleted;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        database = mSqliteOpenHelper.getWritableDatabase();

        int rowsUpdated = 0;

        LogUtils.debug(TAG, "Update rows");

        switch (mUriMatcher.match(uri)) {
            case SINGLE_CATEGORIE:
                rowsUpdated = database.update(PocketBrainContract.Tables.CATEGORIES, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                        " = " + uri.getLastPathSegment() +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            case CATEGORIES:
                rowsUpdated = database.update(PocketBrainContract.Tables.CATEGORIES,
                        values,
                        PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?",
                        selectionArgs);
                break;
            case SINGLE_TAG:
                rowsUpdated = database.update(PocketBrainContract.Tables.TAGS, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case TAGS:
                rowsUpdated = database.update(PocketBrainContract.Tables.TAGS,
                        values,
                        PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?",
                        selectionArgs);
                break;
            case SINGLE_ENTRY:
                rowsUpdated = database.update(PocketBrainContract.Tables.ENTRIES, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case ENTRIES:
                rowsUpdated = database.update(PocketBrainContract.Tables.ENTRIES,
                        values,
                        PocketBrainContract.BASE_COLLUMS._ID + " = ?",
                        selectionArgs);
                break;
            case SINGLE_CONTENT:
                rowsUpdated = database.update(PocketBrainContract.Tables.CONTENT, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case CONTENT:
                rowsUpdated = database.update(PocketBrainContract.Tables.CONTENT,
                        values,
                        PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?",
                        selectionArgs);
                break;
            case SINGLE_CONTAINS:
                rowsUpdated = database.update(PocketBrainContract.Tables.CONTAINS, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case SINGLE_HAS_TAG:
                rowsUpdated = database.update(PocketBrainContract.Tables.HAS_TAGS, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case HAS_TAGS:
                rowsUpdated = database.update(PocketBrainContract.Tables.HAS_TAGS,
                        values,
                        PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?",
                        selectionArgs);
                break;
            case SINGLE_TEXT:
                rowsUpdated = database.update(PocketBrainContract.Tables.TEXT, values,
                        PocketBrainContract.BASE_COLLUMS._ID +
                                " = " + uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ? " AND (" +
                                        selection + ')' : ""), selectionArgs);
                break;
            case TEXT:
                rowsUpdated = database.update(PocketBrainContract.Tables.TEXT,
                        values,
                        PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?",
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI:" +uri);
        }

        if (rowsUpdated > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }


}
