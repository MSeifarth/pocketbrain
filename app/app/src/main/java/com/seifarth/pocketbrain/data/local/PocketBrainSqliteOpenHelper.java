package com.seifarth.pocketbrain.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seifarth.pocketbrain.Config;
import com.seifarth.pocketbrain.utils.LogUtils;

/**
 * Created by seifarth on 30.01.15.
 */
public class PocketBrainSqliteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = PocketBrainSqliteOpenHelper.class.getSimpleName();
    
    private static final String DATABASE_NAME = "pocketbrain.db";
    private static final int CURRENT_DATABASE_VERSION = 1;


    public PocketBrainSqliteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, CURRENT_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        LogUtils.database(TAG,"create Tables");
        
        /* create categorie table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.CATEGORIES + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + PocketBrainContract.CATEGORIES_COLLUMS.NAME + " TEXT NOT NULL,"
                + PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION + " TEXT NOT NULL,"
                + "UNIQUE (" + PocketBrainContract.BASE_COLLUMS._ID + ") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.CATEGORIES));

        /* create entries table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.ENTRIES + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.ENTRIES_COLLUMS.HEADLINE + " TEXT, "
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT, "
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID + " INTEGER NOT NULL,"
              //  + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID +") REFERENCES " + PocketBrainContract.Tables.CATEGORIES + "(" + PocketBrainContract.BASE_COLLUMS._ID + ") ON DELETE CASCADE ON UPDATE CASCADE,"
                + "UNIQUE (" + PocketBrainContract.BASE_COLLUMS._ID + ") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.ENTRIES));
        
        /* create has_tags table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.HAS_TAGS + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + PocketBrainContract.HAS_TAGS_COLLUMS.TAG + " TEXT NOT NULL,"
                + PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID + " INTEGER NOT NULL,"
//                + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID +") REFERENCES " + PocketBrainContract.Tables.ENTRIES + "(" + PocketBrainContract.BASE_COLLUMS._ID + ") ON DELETE CASCADE ON UPDATE CASCADE,"
//                + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.HAS_TAGS_COLLUMS.TAG +") REFERENCES " + PocketBrainContract.Tables.TAGS + "(" + PocketBrainContract.TAG_COLLUMS.NAME + ") ON DELETE CASCADE ON UPDATE CASCADE,"
                + "UNIQUE (" + PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID + "," + PocketBrainContract.HAS_TAGS_COLLUMS.TAG +") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.HAS_TAGS));
        
        
        /* create tag table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.TAGS + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.TAG_COLLUMS.NAME + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + "UNIQUE (" + PocketBrainContract.BASE_COLLUMS._ID + ") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.TAGS));
        
//        /* create contains_link table */
//        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.CONTAINS + " ("
//                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
//                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
//                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
//                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
//                + PocketBrainContract.CONTAINS.CONTENT_ID + " INTEGER NOT NULL,"
//                + PocketBrainContract.CONTAINS.ENTRY_ID + " INTEGER KEY NOT NULL,"
//                + PocketBrainContract.CONTAINS.POSITION + " INTEGER KEY NOT NULL,"
//                + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.CONTAINS.ENTRY_ID +") REFERENCES " + PocketBrainContract.Tables.ENTRIES + "(" + PocketBrainContract.BASE_COLLUMS._ID + ") ON DELETE CASCADE ON UPDATE CASCADE,"
//                + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.CONTAINS.CONTENT_ID +") REFERENCES " + PocketBrainContract.Tables.CONTENT + "(" + PocketBrainContract.BASE_COLLUMS._ID + ") ON DELETE CASCADE ON UPDATE CASCADE,"
//                + "UNIQUE (" + PocketBrainContract.CONTAINS.ENTRY_ID + "," + PocketBrainContract.CONTAINS.CONTENT_ID +") ON CONFLICT REPLACE)");
//        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.CONTAINS));
        
        
        /* create content table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.CONTENT + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID + " INTEGER,"
                + PocketBrainContract.CONTENT_COLLUMS.POSITION + " INTEGER,"
                + PocketBrainContract.CONTENT_COLLUMS.CONTENT_TYPE + " INTEGER NOT NULL,"
                + PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT + " TEXT,"
                + PocketBrainContract.CONTENT_COLLUMS.TYPE_IMAGE_URL + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + "UNIQUE (" + PocketBrainContract.BASE_COLLUMS._ID + ") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.CONTENT));
        
        
        /* create text table */
        db.execSQL("CREATE TABLE " + PocketBrainContract.Tables.TEXT + " ("
                + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"
                + PocketBrainContract.TEXT_COLLUMS.TEXT + " TEXT NOT NULL,"
                + PocketBrainContract.TEXT_COLLUMS.CONTENT_ID + " INTEGER NOT NULL,"
//                + "CONSTRAINT \"PrimaryKey\" FOREIGN KEY( "+ PocketBrainContract.TEXT_COLLUMS.CONTENT_ID +") REFERENCES " + PocketBrainContract.Tables.CONTENT + "(" + PocketBrainContract.BASE_COLLUMS._ID + ") ON DELETE CASCADE ON UPDATE CASCADE,"
                + "UNIQUE (" + PocketBrainContract.BASE_COLLUMS._ID + ") ON CONFLICT REPLACE)");
        LogUtils.database(TAG,String.format("Created Table : %s", PocketBrainContract.Tables.TEXT));


        // create virtual table for full text search
//        db.execSQL("CREATE VIRTUAL TABLE " + PocketBrainContract.Tables.VIRTUAL_TEXT + " USING fts3(" + PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT + " " + ");");
        
        
        LogUtils.database(TAG,"all Tables created");
        if(Config.IS_DATABASE_TEST_DATA) {
            setupDatabaseTestData(db);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LogUtils.database(TAG,"run onUpgrade()");
        LogUtils.database(TAG, "Upgrading from Version " +
                oldVersion + " to " +
                newVersion + " --> destroy all data");
        deleteAllTables(db);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        LogUtils.debug(TAG, "Database opened");
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            LogUtils.database(TAG,"set foreign_keys = ON");
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public synchronized void close() {
        LogUtils.database(TAG, "close Database");
        LogUtils.debug(TAG, "close Database");
        super.close();
    }

    private void deleteAllTables(SQLiteDatabase db) {
        LogUtils.database(TAG,"Delete all Tables");
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.CONTENT);
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.CONTENT);
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.HAS_TAGS);
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.CONTAINS);
        db.execSQL("DROP TABLE IF EXISTS " + PocketBrainContract.Tables.ENTRIES);
    }

    private void setupDatabaseTestData(SQLiteDatabase database) {
        LogUtils.database(TAG,"Create Database Test Content");
        
        //add categories
        ContentValues categorie1 = new ContentValues();
        ContentValues categorie2 = new ContentValues();
        ContentValues categorie3 = new ContentValues();
        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Softwareentwicklung");
        categorie1.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Fachvokabular für Softwareentwicklung, Datenbanken, Requirement Engineering, Tools");
        categorie1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        categorie1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        categorie2.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Usability Engineering");
        categorie2.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Fachvokabular für Usability Engineering");
        categorie2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        categorie2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        categorie3.put(PocketBrainContract.CATEGORIES_COLLUMS.NAME, "Bär");
        categorie3.put(PocketBrainContract.CATEGORIES_COLLUMS.DESCRIPTION,"Eine Kategorie zum testen");
        categorie3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        categorie3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie1);
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie2);
        database.insert(PocketBrainContract.Tables.CATEGORIES, null, categorie3);

        //add tags
        ContentValues tag1 = new ContentValues();
        ContentValues tag2 = new ContentValues();
        ContentValues tag3 = new ContentValues();
        ContentValues tag4 = new ContentValues();
        ContentValues tag5 = new ContentValues();
        ContentValues tag6 = new ContentValues();
        tag1.put(PocketBrainContract.TAG_COLLUMS.NAME,"android");
        tag1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        tag2.put(PocketBrainContract.TAG_COLLUMS.NAME,"datenbanken");
        tag2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        tag3.put(PocketBrainContract.TAG_COLLUMS.NAME,"usability");
        tag3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        tag4.put(PocketBrainContract.TAG_COLLUMS.NAME,"user experience");
        tag4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        tag5.put(PocketBrainContract.TAG_COLLUMS.NAME,"faz");
        tag5.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag5.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        tag6.put(PocketBrainContract.TAG_COLLUMS.NAME,"bär");
        tag6.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        tag6.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, 0);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag1);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag2);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag3);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag4);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag5);
        database.insert(PocketBrainContract.Tables.TAGS, null, tag6);

        //add content
        ContentValues content1 = new ContentValues();
        content1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        ContentValues content2 = new ContentValues();
        content2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        ContentValues content3 = new ContentValues();
        content3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        ContentValues content4 = new ContentValues();
        content4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        database.insert(PocketBrainContract.Tables.CONTENT, null, content1);
        database.insert(PocketBrainContract.Tables.CONTENT, null, content2);
        database.insert(PocketBrainContract.Tables.CONTENT, null, content3);
        database.insert(PocketBrainContract.Tables.CONTENT, null, content4);

//        + PocketBrainContract.BASE_COLLUMS._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + PocketBrainContract.CONTENT_COLLUMS.ENTRIE_ID + " INTEGER,"
//                + PocketBrainContract.CONTENT_COLLUMS.POSITION + " INTEGER,"
//                + PocketBrainContract.CONTENT_COLLUMS.CONTENT_TYPE + " INTEGER NOT NULL,"
//                + PocketBrainContract.CONTENT_COLLUMS.TYPE_TEXT_TEXT + " TEXT,"
//                + PocketBrainContract.CONTENT_COLLUMS.TYPE_IMAGE_URL + " TEXT,"
//                + PocketBrainContract.BASE_COLLUMS.PARSE_ID + " TEXT,"
//                + PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " INTEGER NOT NULL,"
//                + PocketBrainContract.BASE_COLLUMS.CREATED_AT + " INTEGER,"
//                + PocketBrainContract.BASE_COLLUMS.UPDATED_AT + " INTEGER,"



        //add text content
        ContentValues text1 = new ContentValues();
        ContentValues text2 = new ContentValues();
        ContentValues text3 = new ContentValues();
        ContentValues text4 = new ContentValues();
        text1.put(PocketBrainContract.TEXT_COLLUMS.TEXT, "HIER STEHT EIN TEXT FÜR TEXT 1");
        text1.put(PocketBrainContract.TEXT_COLLUMS.CONTENT_ID, 1);
        text1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        text2.put(PocketBrainContract.TEXT_COLLUMS.TEXT, "HIER STEHT EIN TEXT FÜR TEXT 2");
        text2.put(PocketBrainContract.TEXT_COLLUMS.CONTENT_ID, 2);
        text2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        text3.put(PocketBrainContract.TEXT_COLLUMS.TEXT, "HIER STEHT EIN TEXT FÜR TEXT 3");
        text3.put(PocketBrainContract.TEXT_COLLUMS.CONTENT_ID, 3);
        text3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        text4.put(PocketBrainContract.TEXT_COLLUMS.TEXT, "HIER STEHT EIN TEXT FÜR TEXT 4");
        text4.put(PocketBrainContract.TEXT_COLLUMS.CONTENT_ID, 4);
        text4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        database.insert(PocketBrainContract.Tables.TEXT, null, text1);
        database.insert(PocketBrainContract.Tables.TEXT, null, text2);
        database.insert(PocketBrainContract.Tables.TEXT, null, text3);
        database.insert(PocketBrainContract.Tables.TEXT, null, text4);

        //add erntry
        ContentValues entry1 = new ContentValues();
        ContentValues entry2 = new ContentValues();
        ContentValues entry3 = new ContentValues();
        ContentValues entry4 = new ContentValues();
        ContentValues entry5 = new ContentValues();
        entry1.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        entry2.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        entry3.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 1);
        entry3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        entry4.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 2);
        entry4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        entry5.put(PocketBrainContract.ENTRIES_COLLUMS.CATEGORIE_ID, 2);
        entry5.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry1);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry2);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry3);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry4);
        database.insert(PocketBrainContract.Tables.ENTRIES, null, entry5);

        //add tags
        ContentValues hasTag1 = new ContentValues();
        ContentValues hasTag2 = new ContentValues();
        ContentValues hasTag3 = new ContentValues();
        ContentValues hasTag4 = new ContentValues();
        ContentValues hasTag5 = new ContentValues();
        ContentValues hasTag6 = new ContentValues();


        // android <-> entry 1
        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "android");
        hasTag1.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
        hasTag1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // datenbanken <-> entry 1
        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "datenbanken");
        hasTag2.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 1);
        hasTag2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // android <-> entry 2
        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "android");
        hasTag3.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 2);
        hasTag3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // datenbanken <-> entry 2
        hasTag4.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "datenbanken");
        hasTag4.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 2);
        hasTag4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // faz <-> entry (person) 4
        hasTag5.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "faz");
        hasTag5.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 4);
        hasTag5.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // itemis <-> entry (person) 5
        hasTag6.put(PocketBrainContract.HAS_TAGS_COLLUMS.TAG, "itemis");
        hasTag6.put(PocketBrainContract.HAS_TAGS_COLLUMS.ENTRY_ID, 5);
        hasTag6.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag2);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag1);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag3);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag4);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag5);
        database.insert(PocketBrainContract.Tables.HAS_TAGS, null, hasTag6);


        //add links
        ContentValues contains1 = new ContentValues();
        ContentValues contains2 = new ContentValues();
        ContentValues contains3 = new ContentValues();
        ContentValues contains4 = new ContentValues();


        // content1 <-> entry 1
        contains1.put(PocketBrainContract.CONTAINS.ENTRY_ID, 1);
        contains1.put(PocketBrainContract.CONTAINS.CONTENT_ID, 1);
        contains1.put(PocketBrainContract.CONTAINS.POSITION,1);
        contains1.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // content2 <-> entry 1
        contains2.put(PocketBrainContract.CONTAINS.ENTRY_ID, 1);
        contains2.put(PocketBrainContract.CONTAINS.CONTENT_ID, 2);
        contains2.put(PocketBrainContract.CONTAINS.POSITION,2);
        contains2.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // content1 <-> entry 2
        contains3.put(PocketBrainContract.CONTAINS.ENTRY_ID, 1);
        contains3.put(PocketBrainContract.CONTAINS.CONTENT_ID, 3);
        contains3.put(PocketBrainContract.CONTAINS.POSITION,3);
        contains3.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        // content3 <-> entry 3
        contains4.put(PocketBrainContract.CONTAINS.ENTRY_ID, 3);
        contains4.put(PocketBrainContract.CONTAINS.CONTENT_ID, 4);
        contains4.put(PocketBrainContract.CONTAINS.POSITION,1);
        contains4.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);

        database.insert(PocketBrainContract.Tables.CONTAINS, null, contains1);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, contains2);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, contains3);
        database.insert(PocketBrainContract.Tables.CONTAINS, null, contains4);

        LogUtils.database(TAG,"Database Testcontent created..");
    }

}
