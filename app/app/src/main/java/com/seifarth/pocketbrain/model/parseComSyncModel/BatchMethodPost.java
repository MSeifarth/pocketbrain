package com.seifarth.pocketbrain.model.parseComSyncModel;

import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseBatchMethods;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;

/**
 * Created by seifarth on 07.03.15.
 */
public class BatchMethodPost extends BaseBatchMethods {

    private String method = "POST";
    private String path;
    private BaseParseComObjectMapper body;

    public BatchMethodPost(String className, BaseParseComObjectMapper methodObject) {
        this.body = methodObject;
        this.path = "/1/classes/"+className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public BaseParseComObjectMapper getBody() {
        return body;
    }

    public void setBody(BaseParseComObjectMapper body) {
        this.body = body;
    }
}
