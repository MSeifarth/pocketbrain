package com.seifarth.pocketbrain.data;

import android.app.Application;

import com.securepreferences.SecurePreferences;
import com.seifarth.pocketbrain.data.local.PocketBrainContentProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false,
        library = true
)
public class PersistenceModule {

    @Provides
    @Singleton
    public SecurePreferences provideSecurePreferences(Application application) {
        return new SecurePreferences(application);
    }

    public PocketBrainContentProvider provideContentProvider() {
        return new PocketBrainContentProvider();
    }
}
