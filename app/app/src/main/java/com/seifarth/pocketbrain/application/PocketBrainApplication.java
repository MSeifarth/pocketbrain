package com.seifarth.pocketbrain.application;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.securepreferences.SecurePreferences;
import com.seifarth.pocketbrain.Config;
import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.gcm.GcmRegisterer;
import com.seifarth.pocketbrain.model.User;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by seifarth on 21.02.15.
 */
public class PocketBrainApplication extends Application {

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    private static final String SENDER_ID = "618939617699";
    private static final String PROPERTY_ID = "UA-61060260-1";

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public static User user = new User();
    public static String authToken;

    public ObjectGraph objectGraph;

    @Override public void onCreate() {
        super.onCreate();

        if(Config.RESET_SYNC_DATA_STATE) {
            SecurePreferences mSecurePrefrence = new SecurePreferences(this);
            mSecurePrefrence.edit().putString("lastSyncDate","2000-01-01T00:00:00Z").commit();
        }

        objectGraph = ObjectGraph.create(getModules().toArray());
        objectGraph.inject(this);
        GcmRegisterer gcmRegisterer = new GcmRegisterer(this,new SecurePreferences(this), SENDER_ID);
        String regId = gcmRegisterer.register();
        initStetho();
    }

    private void initStetho() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }

    private List<Object> getModules() {
        return Arrays.<Object>asList(new ApplicationModule(this));
    }

    public static PocketBrainApplication get(Context context) {
        return (PocketBrainApplication) context.getApplicationContext();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : analytics.newTracker(R.xml.app_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }
}
