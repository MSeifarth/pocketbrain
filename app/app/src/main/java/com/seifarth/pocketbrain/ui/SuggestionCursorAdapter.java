package com.seifarth.pocketbrain.ui;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifarth.pocketbrain.R;
import com.seifarth.pocketbrain.data.local.PocketBrainSuggestionProvider;


/**
 * Just a litlte customized CursorAdapter for the search suggestions.
 * Highlights the current query state in the suggestion texts.
 */
public class SuggestionCursorAdapter extends CursorAdapter {

    private Context context;

    public SuggestionCursorAdapter(Context context, Cursor c, int flags, Context context1) {
        super(context, c, flags);
        this.context = context1;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        return inflater.inflate(R.layout.row_suggestion, parent,false);
    }

    @SuppressLint("NewApi")
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView imageView = (ImageView) view.findViewById(R.id.suggest_iv_icon);
        imageView.setImageDrawable(context.getDrawable(cursor.getInt(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_ICON_1))));

        TextView textView = (TextView) view.findViewById(R.id.suggest_tv_text);
        Spannable wordToSpan = getSpannendSuggestionText(cursor);
        textView.setText(wordToSpan, TextView.BufferType.SPANNABLE);
    }

    /**
     * Returns a spannend version of the suggestion text. The current query is highlighted with
     * color in the suggestion text.
     *
     * @param cursor cursor showing on current element
     * @return the spannend text
     */
    private Spannable getSpannendSuggestionText(Cursor cursor) {
        String currentQuery = cursor.getString(cursor.getColumnIndex(PocketBrainSuggestionProvider.CURRENT_QUERY));
        String suggestedText = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));

        Spannable wordToSpan = new SpannableString(suggestedText);
        int startIndex = suggestedText.toLowerCase().indexOf(currentQuery.toLowerCase());
        wordToSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.pocket_brain_pink)),
                startIndex,
                startIndex + currentQuery.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return wordToSpan;
    }
}
