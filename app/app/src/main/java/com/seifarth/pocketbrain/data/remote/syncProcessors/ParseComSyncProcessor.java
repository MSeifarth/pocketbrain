package com.seifarth.pocketbrain.data.remote.syncProcessors;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.securepreferences.SecurePreferences;
import com.seifarth.pocketbrain.authentication.PocketBrainParserServer;
import com.seifarth.pocketbrain.data.local.PocketBrainContract;
import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseResponseToOperationMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncMapper;
import com.seifarth.pocketbrain.data.remote.syncProcessors.base.BaseSyncProcessor;
import com.seifarth.pocketbrain.model.parseComSyncModel.BatchMethodPost;
import com.seifarth.pocketbrain.model.parseComSyncModel.BatchMethodPut;
import com.seifarth.pocketbrain.model.parseComSyncModel.BatchRequestBody;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseBatchMethods;
import com.seifarth.pocketbrain.model.parseComSyncModel.base.BaseParseComObjectMapper;
import com.seifarth.pocketbrain.utils.LogUtils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by seifarth on 28.02.15.
 */
public class ParseComSyncProcessor extends BaseSyncProcessor {

    private static final String TAG = ParseComSyncProcessor.class.getSimpleName();

    private final String PARSE_COM_BASE_URL = "https://api.parse.com/1/";
    private final String PARSE_COM_BASE_QUERY_URL = "classes/%s?where=";
    private final String GET_INSERTED_DATA_QUERY = "{\"updatedAt\":{\"$gte\":{\"__type\":\"Date\",\"iso\":\"%s\"}},\"createdAt\":{\"$gte\":{\"__type\":\"Date\",\"iso\":\"%s" +
            "\"}},\"deleted\":{\"$ne\":1}}";

    private Uri contentUri;

    private ContentProviderClient mProviderClient;
    private String classToSyncName;
    private String queryUrl;
    private BaseResponseToOperationMapper responseToOperationsMapper;
    private BaseParseComObjectMapper baseParseComObjectMapper;

    @Inject
    OkHttpClient mOkHttpClient;

    @Inject
    SecurePreferences mSecurePrefs;

    @Inject
    Gson mGson;

    @Inject
    JsonParser mParser;

    public ParseComSyncProcessor(Context context, ContentProviderClient contentProviderClient,BaseSyncMapper syncMapper) {
        super(context);
        this.mProviderClient = contentProviderClient;
        this.responseToOperationsMapper = syncMapper.getReponseToOperationMapper();
        this.baseParseComObjectMapper = syncMapper.getParserObjectMapper();
        this.queryUrl = String.format(PARSE_COM_BASE_QUERY_URL, syncMapper.getClassToSync());
        this.contentUri = responseToOperationsMapper.getContentUri();
        classToSyncName = syncMapper.getClassToSync();
    }

    @Override
    public boolean syncData() {
        LogUtils.sync(TAG, String.format("----- Syncing %s ---------------------------", classToSyncName));
        return super.syncData();
    }

    @Override
    protected boolean syncInsertedDataFromServer() {
        boolean isSuccessfull = true;

        String lastSync = mSecurePrefs.getString("lastSyncDate", "2000-01-01T00:00:00Z");
        LogUtils.sync(TAG, String.format("----------- Sync inserted Data From Server : %s -------------",classToSyncName));

        List<ContentProviderOperation> operationsToUpdate = new ArrayList<ContentProviderOperation>();

        try {
            final String encodedRequestParams = URLEncoder.encode(String.format(GET_INSERTED_DATA_QUERY,lastSync,lastSync), "UTF-8");

            final Request request = new Request.Builder()
                    .url(PARSE_COM_BASE_URL + queryUrl +encodedRequestParams) // e.g. https://api.parse.com/1/classes/categories?where=#
                    .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                            .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                            .get()
                            .build();
            final Response response = mOkHttpClient.newCall(request).execute();

            if(response.isSuccessful()) {
                operationsToUpdate = responseToOperationsMapper.mapForInsert(response,mParser);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!operationsToUpdate.isEmpty()) {
            try {
                LogUtils.sync(TAG, "-------------- Update rows to database: "+operationsToUpdate.size());
                mProviderClient.applyBatch((ArrayList)operationsToUpdate);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }
        } else {
            LogUtils.sync(TAG, "-------------- No Rows Updated in local Database ");
        }

        return isSuccessfull;
    }

    @Override
    protected boolean syncInsertedDataToServer() {
        boolean isSuccesful = true;
        LogUtils.sync(TAG, "----------- Sync inserted Data to Server -------------");

        ArrayList<ContentProviderOperation> operationsToUpdate = new ArrayList<ContentProviderOperation>();
        
        try {
            final Cursor cursor = mProviderClient.query(contentUri,
                    null,
                    PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " = " + PocketBrainContract.IS_INSERTING,
                    null,
                    null);

            if(cursor.getCount() != 0) {
                LogUtils.sync(TAG, String.format("----------- %s rows to insert to Server", String.valueOf(cursor.getCount())));

                final Set<BaseBatchMethods> batchRequests = new LinkedHashSet<BaseBatchMethods>();
                cursor.moveToFirst();
                final ArrayList<Integer> insertedRows = new ArrayList<Integer>();
                while(!cursor.isAfterLast()) {
                    batchRequests.add(new BatchMethodPost(classToSyncName, baseParseComObjectMapper.fromCursor(cursor)));
                    insertedRows.add(cursor.getInt(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS._ID)));
                    cursor.moveToNext();
                }
                final BatchRequestBody batchRequestBody = new BatchRequestBody(batchRequests);

                final RequestBody body = RequestBody.create(MediaType.parse("application/json"), mGson.toJson(batchRequestBody));
                final Request request = new Request.Builder()
                        .url(PARSE_COM_BASE_URL +"batch")
                        .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                        .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                        .post(body)
                        .build();

                final Response response = mOkHttpClient.newCall(request).execute();

                if(response.isSuccessful()) {
                    LogUtils.sync(TAG,String.format("Successfully inserted %s objects to server", cursor.getCount()));

                    operationsToUpdate = responseToOperationsMapper.mapForInsertedToServer(insertedRows,response,mParser);

                    if(!operationsToUpdate.isEmpty()) {
                        try {
                            LogUtils.sync(TAG, "-------------- Update rows to database: "+operationsToUpdate.size());
                            mProviderClient.applyBatch((ArrayList)operationsToUpdate);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        } catch (OperationApplicationException e) {
                            e.printStackTrace();
                        }
                    } else {
                        LogUtils.sync(TAG, "-------------- No Rows Updated in local Database ");
                    }

                }

            } else {
                LogUtils.sync(TAG,String.format("-------------- nothing to Sync in %s",classToSyncName));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isSuccesful;
    }


    @Override
    protected boolean syncUpdatedDataFromServer() {
        boolean isSuccessfull = true;

        LogUtils.sync(TAG, "----------- Sync updated Data From Server -------------");

        final String lastSync = mSecurePrefs.getString("lastSyncDate", "2000-01-01T00:00:00Z");

        ArrayList<ContentProviderOperation> operationsToUpdate = new ArrayList<ContentProviderOperation>();

        try {
            final String encodedUrl = URLEncoder.encode(String.format("{\"updatedAt\":{\"$gte\":{\"__type\":\"Date\",\"iso\":\"%s\"}},\"createdAt\":{\"$lte\":{\"__type\":\"Date\",\"iso\":\"%s\"}},\"deleted\":{\"$ne\":1}}",lastSync,lastSync), "UTF-8");

            final Request request = new Request.Builder()
                    .url(PARSE_COM_BASE_URL +queryUrl+ encodedUrl)
                    .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                    .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                    .get()
                    .build();
            final Response response = mOkHttpClient.newCall(request).execute();

            if(response.isSuccessful()) {
                operationsToUpdate = responseToOperationsMapper.mapForUpdate(response,mParser);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!operationsToUpdate.isEmpty()) {
            try {
                LogUtils.sync(TAG, String.format("-------------- %s Rows updated to local db", operationsToUpdate.size()));
                ContentProviderResult[] contentProviderResults = mProviderClient.applyBatch(operationsToUpdate);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }
        }
        return isSuccessfull;
    }

    @Override
    protected boolean syncDeletedDataToServer() {
        boolean isSuccesful = true;

        LogUtils.sync(TAG, "----------- Sync Deleted Data to Server -------------");

        try {
            final Cursor cursor = mProviderClient.query(contentUri,
                    null,
                    PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " = ?",
                    new String[]{String.valueOf(PocketBrainContract.IS_DELETING)},
                    null);
            
            if(cursor.getCount() != 0) {
                LogUtils.sync(TAG, "-------------- Rows to delete on Server :  " + cursor.getCount());

                final Set<BaseBatchMethods> batchRequests = new LinkedHashSet<BaseBatchMethods>();
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    batchRequests.add(new BatchMethodPut(classToSyncName,cursor.getString(cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.PARSE_ID)), baseParseComObjectMapper.fromCursor(cursor)));
                    cursor.moveToNext();
                }
                
                final BatchRequestBody batchRequestBody = new BatchRequestBody(batchRequests);
                
                final RequestBody body = RequestBody.create(MediaType.parse("application/json"), mGson.toJson(batchRequestBody));
                final Request request = new Request.Builder()
                        .url(PARSE_COM_BASE_URL +"batch")
                        .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                        .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                        .post(body)
                        .build();

                final Response response = mOkHttpClient.newCall(request).execute();

                if(response.isSuccessful()) {
                    // delete all rows that was marked as deleting
                    int rowsDeleted = mProviderClient.delete(contentUri,
                            PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " = ?",
                            new String[]{String.valueOf(PocketBrainContract.IS_DELETING)});

                    if(cursor.getCount() != rowsDeleted) {
                        LogUtils.sync(TAG,"--------------  Rows local deleted are not equals to Rows deleted remote");
                        isSuccesful = false;
                    }
                    LogUtils.sync(TAG,String.format("--------------  Successfully deleted %s from local", rowsDeleted));


                } else {
                    LogUtils.sync(TAG,"Something wrong with sync to server");
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isSuccesful;
    }

    @Override
    protected boolean syncDeleteDataFromServer() {
        boolean isSuccesful = true;

        LogUtils.sync(TAG, "----------- Sync Deleted Data from Server -------------");

        final String lastSync = mSecurePrefs.getString("lastSyncDate", "2000-01-01T00:00:00Z");

        final ArrayList<ContentProviderOperation> operationsToUpdate = new ArrayList<ContentProviderOperation>();

        try {
            final String encodedUrl = URLEncoder.encode(String.format("{\"updatedAt\":{\"$gte\":{\"__type\":\"Date\",\"iso\":\"%s\"}},\"deleted\":1}}",lastSync), "UTF-8");

            final Request request = new Request.Builder()
                    .url(PARSE_COM_BASE_URL + queryUrl + encodedUrl)
                    .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                    .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                    .get()
                    .build();
            final Response response = mOkHttpClient.newCall(request).execute();

            if(response.isSuccessful()) {
                final String requestBody = response.body().string();
                final JsonArray resultsElements = mParser.parse(requestBody).getAsJsonObject().getAsJsonArray("results");
                final Iterator<JsonElement> iterator = resultsElements.iterator();
                LogUtils.sync(TAG, "-------------- Rows to delete:" + resultsElements.size());

                while (iterator.hasNext()) {
                    JsonObject jsonObject = iterator.next().getAsJsonObject();
                    operationsToUpdate.add(ContentProviderOperation.newDelete(
                            contentUri)
                            .withSelection(PocketBrainContract.BASE_COLLUMS.PARSE_ID + " = ?", new String[]{jsonObject.get("objectId").getAsString()})
                            .build());
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!operationsToUpdate.isEmpty()) {
            try {
                mProviderClient.applyBatch(operationsToUpdate);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }
        }
        
        return isSuccesful;
    }

    protected boolean syncUpdatedDataToServer() {
        boolean isSuccessful = true;
        LogUtils.sync(TAG, "----------- Sync Updated Data to Server -------------");

        try {
            final Cursor cursor = mProviderClient.query(contentUri,
                    null,
                    PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " = " + PocketBrainContract.IS_UPDATING,
                    null,
                    null);

            if(cursor.getCount() != 0) {
                LogUtils.sync(TAG, String.format("-------------- %s rows to update to Server", String.valueOf(cursor.getCount())));

                final Set<BaseBatchMethods> batchRequests = new LinkedHashSet<BaseBatchMethods>();
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    batchRequests.add(new BatchMethodPut(classToSyncName,cursor.getString(
                            cursor.getColumnIndex(PocketBrainContract.BASE_COLLUMS.PARSE_ID)),
                            baseParseComObjectMapper.fromCursor(cursor)));
                    cursor.moveToNext();
                }
                final BatchRequestBody batchRequestBody = new BatchRequestBody(batchRequests);

                final RequestBody body = RequestBody.create(MediaType.parse("application/json"), mGson.toJson(batchRequestBody));
                final Request request = new Request.Builder()
                        .url(PARSE_COM_BASE_URL +"batch")
                        .addHeader("X-Parse-Application-Id", PocketBrainParserServer.PARSE_SERVER_APP_ID)
                        .addHeader("X-Parse-REST-API-Key", PocketBrainParserServer.PARSE_REST_API_KEY)
                        .post(body)
                        .build();
    
                final Response response = mOkHttpClient.newCall(request).execute();
                
                if(response.isSuccessful()) {
                    LogUtils.sync(TAG,String.format("-------------- Successfully sync %s objects to server", cursor.getCount()));

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE, PocketBrainContract.DEFAULT);
                    
                    //reset DB tr_state to 0;
                    int update = mProviderClient.update(
                            contentUri,
                            contentValues,
                            PocketBrainContract.BASE_COLLUMS.TRANSACTION_STATE + " = ?",
                            new String[]{String.valueOf(PocketBrainContract.IS_UPDATING)});
    
                } else {
                    LogUtils.sync(TAG,"Something wrong with sync to server");
                }

            } else {
                LogUtils.sync(TAG,String.format("-------------- nothing to Sync in %s",classToSyncName));
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isSuccessful;
    }
}
