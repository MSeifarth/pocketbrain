package com.seifarth.pocketbrain.data.local;

import android.app.SearchManager;
import android.net.Uri;

/**
 * Created by seifarth on 30.01.15.
 */
public class PocketBrainContract {

    public static final String AUTHORITY = "com.seifarth.provider.pocketbrain";

    // pocketbrain content URIs
    // base
    public static final String BASE_URI = "content://com.seifarth.provider.pocketbrain";
    public static final String BASE_URI_SEARCH = "content://com.seifarth.provider.pocketbrainsuggestions";
    public static final String BASE_URI_RECENT_SEARCH = "content://com.seifarth.provider.pocketbrainrecentsuggestions";
    public static final Uri CONTENT_URI_CATEOGIRES = Uri.parse(BASE_URI + "/categories");
    public static final Uri CONTENT_URI_ENTRIES = Uri.parse(BASE_URI + "/entries");
    public static final Uri CONTENT_URI_HAS_TAGS = Uri.parse(BASE_URI + "/hastags");
    public static final Uri CONTENT_URI_TAGS = Uri.parse(BASE_URI + "/tags");
    public static final Uri CONTENT_URI_CONTAIN = Uri.parse(BASE_URI + "/contain");
    public static final Uri CONTENT_URI_CONTENT = Uri.parse(BASE_URI + "/content");
    public static final Uri CONTENT_URI_TEXT = Uri.parse(BASE_URI + "/text");

    // search Uri
    public static final Uri CONTENT_URI_SEARCH = Uri.parse(BASE_URI_SEARCH + "/searchMe/"+ SearchManager.SUGGEST_URI_PATH_QUERY);
    public static final Uri CONTENT_URI_RECENT_SEARCH = Uri.parse(BASE_URI_RECENT_SEARCH + "/suggestions");

    //enriched
    public static final Uri CONTENT_URI_ENTRIES_WITH_TAGS = Uri.parse(BASE_URI + "/entriesWithTags");

    // pocketbrain MIME types
    // base types
    public static final String CATEGOIES_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.categories";
    public static final String SINGLE_CATEGOIE_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.categories";
    public static final String ENTRIES_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.entries";
    public static final String SINGLE_ENTRY_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.entries";
    public static final String HAS_TAGS_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.hastags";
    public static final String SINGLE_HAS_TAG_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.hastags";
    public static final String TAGS_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.tags";
    public static final String SINGLE_TAG_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.tags";
    public static final String CONTAINS_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.contains";
    public static final String SINGLE_CONTAINS_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.contains";
    public static final String TEXT_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.texts";
    public static final String SINGLE_TEXT_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.texts";
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.content";
    public static final String SINGLE_CONTENT_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.content";

    public static final String SEARCH_RESULTS_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.content";

    //enriched types
    public static final String ENTRIES_WITH_TAGS_TYPE = "vnd.android.cursor.dir / vnd.seifarth.pocketbrain.entiresWithTags";
    public static final String SINGLE_ENTRY_WITH_TAGS_TYPE = "vnd.android.cursor.item / vnd.seifarth.pocketbrain.entriesWithTags";
    public static final String ENTRIES_DEFAULT_SORT_ORDER = ENTRIES_COLLUMS.HEADLINE;


    // databse schema
    public interface Tables {
        String CATEGORIES = "categories";
        String ENTRIES = "entries";
        String HAS_TAGS = "has_tags";
        String TAGS = "tags";
        String CONTAINS = "contains";
        String CONTENT = "content";
        String TEXT = "text";
        String VIRTUAL_TEXT = "virtual_text";
    }

    public interface BASE_COLLUMS {
        String _ID = "_id";
        String TRANSACTION_STATE = "transState";
        String CREATED_AT = "createdAt";
        String UPDATED_AT = "updatedAt";
        String PARSE_ID = "objectId";
    }

    public interface CATEGORIES_COLLUMS{
        String NAME = "name";
        String DESCRIPTION = "description";
    }

    public interface ENTRIES_COLLUMS{
        String HEADLINE = "headline";
        String BODY = "body";
        String CREATED = "created";
        String MODIFIED = "modified";
        String CATEGORIE_ID = "categorieId";
    }

    public interface HAS_TAGS_COLLUMS {
        String ENTRY_ID ="entryId";
        String TAG = "tag";
    }

    public interface TAG_COLLUMS {
        String NAME = "name";
    }

    public interface CONTAINS {
        String ENTRY_ID = "entryId";
        String CONTENT_ID = "contentId";
        String POSITION = "position";
    }
    
    public interface CONTENT_COLLUMS {
        String ENTRIE_ID = "entryId";
        String POSITION = "position";
        String CONTENT_TYPE = "type";
        String TYPE_TEXT_TEXT = "text_text";
        String TYPE_IMAGE_URL = "image_url";
    }
    
    public interface TEXT_COLLUMS {
        String CONTENT_ID = "contentId";
        String TEXT = "text";
    }
    
    // TRANSACTION_STATES
    public static final int DEFAULT = 0;
    public static final int IS_INSERTING = 1;
    public static final int IS_UPDATING = 2;
    public static final int IS_DELETING = 3;

    // Content Types
    public interface CONTENT_TYPES {
        int CONTENT_TYPE_TEXT = 0;
        int CONTENT_TYPE_IMAGE = 1;
    }

}
