package com.seifarth.pocketbrain.utils;

import android.util.Log;

import com.seifarth.pocketbrain.Config;

/**
 * Created by seifarth on 29.01.15.
 */
public class LogUtils {

    private static final String DEBUG_TAG = "DEBUG";
    private static final String DATABASE_TAG = "DATABASE";
    private static final String TEST_TAG = "TEST";
    private static final String CONTENT_PROVIDER_TAG = "CONTENT_PROVIDER";
    private static final String SYNC = "SYNC";


    public static void debug(String tag, String msg) {
        if(Config.IS_DEVELOP) {
            Log.d(DEBUG_TAG, " " + tag + " --> " + msg);
        }
    }
    
    public static void database(String tag, String msg) {
        if(Config.IS_DEVELOP) {
            Log.d(DATABASE_TAG, " " + tag + " --> " + msg);
        }
    }
    public static void sync(String tag, String msg) {
        if(Config.IS_DEVELOP) {
            Log.d(SYNC, " " + tag + " --> " + msg);
        }
    }

    public static void test(String tag, String msg) {
        if(Config.IS_DEVELOP) {
            Log.d(TEST_TAG, " " + tag + " --> " + msg);
        }
    }
}
