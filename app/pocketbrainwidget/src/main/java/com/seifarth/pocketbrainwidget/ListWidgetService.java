package com.seifarth.pocketbrainwidget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

/**
 * Created by seifarth on 26.03.15.
 */
public class ListWidgetService extends RemoteViewsService {

    private static final String TAG = ListWidgetService.class.getSimpleName();

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        Uri providerUri = intent.getData();
        Log.d(TAG,"content uri ist : "+providerUri.toString());
        Cursor cursor = getContentResolver().query(providerUri, null, null, null, null);

        return new ListRemoteViewsFactory(cursor,getApplicationContext(),intent);
    }


    /**
     * Thin wrapper around the Adapter interface in context of the app widget.
     */
    class ListRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

        private final String TAG = ListRemoteViewsFactory.class.getSimpleName();
        private Cursor mCursor;
        private Context mContext;
        private int mAppWidgetId;

        public ListRemoteViewsFactory(Cursor cursor, Context applicationContext, Intent intent) {
            this.mCursor = cursor;
            this.mContext = applicationContext;
            mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        @Override
        public void onCreate() {
            Log.d(TAG, "ViewsFactory onCreate");
        }

        @Override
        public void onDataSetChanged() {
            Log.d(TAG, "ViewsFactory dataSetChange");

        }

        @Override
        public void onDestroy() {
            Log.d(TAG, "ViewsFactory onDestroy");
            mCursor.close();
        }

        @Override
        public int getCount() {
            Log.d(TAG, "ViewsFactory getCount --> " + mCursor.getCount());
            return mCursor.getCount();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            mCursor.moveToPosition(position);
            String text = mCursor.getString(mCursor.getColumnIndex("headline"));
            RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widget_row);
            remoteViews.setTextViewText(R.id.widgettext, text);

            // Next, set a fill-intent, which will be used to fill in the pending intent template
            // that is set on the collection view in StackWidgetProvider.
            Bundle extras = new Bundle();
            extras.putInt(MyWidgetProvider.EXTRA_ITEM, position);
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);
            // Make it possible to distinguish the individual on-click
            // action of a given item
            remoteViews.setOnClickFillInIntent(R.id.widget_item, fillInIntent);


            return remoteViews;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}
