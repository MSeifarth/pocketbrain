package com.seifarth.pocketbrainwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;


/**
 * Created by seifarth on 26.03.15.
 */
public class MyWidgetProvider extends AppWidgetProvider{

    private static final String TAG = MyWidgetProvider.class.getSimpleName();

    public static final String TOAST_ACTION = "com.example.android.stackwidget.TOAST_ACTION";
    public static final String EXTRA_ITEM = "com.example.android.stackwidget.EXTRA_ITEM";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "pocketbrain widget onReceive");

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        if(intent.getAction().equals(TOAST_ACTION)) {
            int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);
            Toast.makeText(context, "Touched view " + viewIndex, Toast.LENGTH_SHORT).show();
        }

        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "pocketbrain widget onUpdate");


        ComponentName thisWidget = new ComponentName(context, MyWidgetProvider.class);

        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        Log.d(TAG, String.format("Found %s Widget of this type", allWidgetIds.length));


        for (int widgetId : allWidgetIds) {
            Intent intent = new Intent(context, ListWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            intent.setData(Uri.parse("content://com.seifarth.provider.pocketbrain/entries"));

            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            rv.setRemoteAdapter(R.id.listView, intent);
            rv.setEmptyView(R.id.listView, R.layout.widget_row);

            Intent activityStartIntent = context.getPackageManager().getLaunchIntentForPackage("com.seifarth.pocketbrain");
            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,activityStartIntent,0);
            rv.setOnClickPendingIntent(R.id.imageView, pendingIntent);



            Intent toastIntent = new Intent(context, MyWidgetProvider.class);
            toastIntent.setAction(MyWidgetProvider.TOAST_ACTION);
            toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            toastIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
            PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(R.id.listView, toastPendingIntent);




            appWidgetManager.updateAppWidget(widgetId,rv);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.d(TAG, "pocketbrain widget onDelete");
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        Log.d(TAG, "pocketbrain widget onEnable");
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        Log.d(TAG, "pocketbrain widget onDisble");
        super.onDisabled(context);
    }

    @Override
    public void onRestored(Context context, int[] oldWidgetIds, int[] newWidgetIds) {
        Log.d(TAG, "pocketbrain widget onRestored");
        super.onRestored(context, oldWidgetIds, newWidgetIds);
    }
}
